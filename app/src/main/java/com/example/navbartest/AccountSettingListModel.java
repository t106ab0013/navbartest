package com.example.navbartest;

public class AccountSettingListModel {

    int imageicon;
    String settingText;
    int imagenext;

    public AccountSettingListModel(int imageicon, String settingText, int imagenext) {
        this.imageicon = imageicon;
        this.settingText = settingText;
        this.imagenext = imagenext;
    }

    public int getImageicon() {
        return imageicon;
    }

    public void setImageicon(int imageicon) {
        this.imageicon = imageicon;
    }

    public String getSettingText() {
        return settingText;
    }

    public void setSettingText(String settingText) {
        this.settingText = settingText;
    }

    public int getImagenext() {
        return imagenext;
    }

    public void setImagenext(int imagenext) {
        this.imagenext = imagenext;
    }
}
