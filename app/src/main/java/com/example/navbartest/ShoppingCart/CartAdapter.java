package com.example.navbartest.ShoppingCart;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.navbartest.Activity.CommodityBrowserActivity;
import com.example.navbartest.Common.DialogHelper;
import com.example.navbartest.Fragment.NotificationsFragment;
import com.example.navbartest.GlideApp;
import com.example.navbartest.Item.Commodity;
import com.example.navbartest.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartViewHolder> {

    Context context;
    List<Commodity> itemlist;
    List<String> addCart;
    String userID;

    StorageReference itemImageRef;
    DatabaseReference reAdd;

    public CartAdapter(Context context,List<Commodity> itemlist,List<String> addCart,String userID){
        this.context = context;
        this.itemlist = itemlist;
        this.addCart = addCart;
        this.userID = userID;
        reAdd = FirebaseDatabase.getInstance().getReference().child("AccountFile").child(userID);
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.shoppingcart_layout,parent,false);

        return new CartViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CartViewHolder holder, final int position) {
        itemImageRef = FirebaseStorage.getInstance().getReference().child("CommodityPhoto").child(itemlist.get(position).getItemID()).child("1");

        GlideApp.with(context)
                .load(itemImageRef)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .placeholder(R.drawable.ic_image_white_24dp)
                .centerInside()
                .into(holder.ItemImage);

        holder.tvItemName.setText(itemlist.get(position).getItemName());
        holder.tvItemPrice.setText("$ "+itemlist.get(position).getItemPrice());
        holder.tvItemLocation.setText("商品位於 "+itemlist.get(position).getItemLocation());
        if (itemlist.get(position).getlend()){
            holder.tVlend.setText("商品出借中...");
        }
        else{
            holder.tVlend.setText("商品可供租借");
        }


        holder.CartItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, CommodityBrowserActivity.class);
                i.putExtra("ItemID",itemlist.get(position).getItemID());
                i.putExtra("ImgCount",itemlist.get(position).getImgCount());
                i.putExtra("ItemOwner",itemlist.get(position).getItemOwner());
                i.putExtra("lend",itemlist.get(position).getlend());
                context.startActivity(i);
            }
        });

        holder.CartItem.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                DialogHelper.simpleDialog2Listener(context, "", "確定要刪除商品嗎", "確定", "取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        removeData(position);
                        updateCartDB(position);
                    }
                }, null);
                return true;
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemlist.size();
    }

    public void removeData(int position){
        itemlist.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    public void updateCartDB(int position){
        addCart.remove(position);
        HashMap<String,Object> reupload = new HashMap<>();
        reupload.put("cart",addCart);
        reAdd.updateChildren(reupload).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(context,"已移除該商品",Toast.LENGTH_SHORT).show();
            }
        });
    }

}
