package com.example.navbartest.ShoppingCart;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.navbartest.R;

public class CartViewHolder extends RecyclerView.ViewHolder {

    LinearLayout CartItem;
    ImageView ItemImage;
    TextView tvItemName,tvItemPrice,tvItemLocation,tVlend;

    public CartViewHolder(@NonNull View itemView) {
        super(itemView);
        CartItem = itemView.findViewById(R.id.CartItem);
        ItemImage = itemView.findViewById(R.id.ItemImage);
        tvItemName = itemView.findViewById(R.id.tvItemName);
        tvItemPrice = itemView.findViewById(R.id.tvItemPrice);
        tvItemLocation = itemView.findViewById(R.id.tvItemLocation);
        tVlend = itemView.findViewById(R.id.tVlend);
    }
}
