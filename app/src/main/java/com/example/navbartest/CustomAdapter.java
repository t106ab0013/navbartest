package com.example.navbartest;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.navbartest.Activity.search_activity;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder>  {



    List<SchoolTagModel> mList;
    Context context;

    public CustomAdapter(List<SchoolTagModel> mList, Context context) {
        this.mList = mList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater= LayoutInflater.from(context);
        View view=layoutInflater.inflate(R.layout.layout_file,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        GlideApp.with(context).load(mList.get(position).getImage()).into(holder.imageView);
        holder.textView.setText(mList.get(position).getText());
        holder.schoolModel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), search_activity.class);
                i.putExtra("schoolName",mList.get(position).getText());
                view.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder  {
        ImageView imageView;
        TextView textView;
        ConstraintLayout schoolModel;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            schoolModel = itemView.findViewById(R.id.schoolModel);
            imageView=itemView.findViewById(R.id.layoutimageid);
            textView=itemView.findViewById(R.id.Textviewid);

              }
          };




    }

