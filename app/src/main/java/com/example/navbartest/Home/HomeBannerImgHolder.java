package com.example.navbartest.Home;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.example.navbartest.GlideApp;
import com.example.navbartest.R;
import com.zhpan.bannerview.BaseViewHolder;

public class HomeBannerImgHolder extends BaseViewHolder<Integer> {

    public HomeBannerImgHolder(@NonNull View itemView){
        super(itemView);
        ImageView itemImage = findView(R.id.itemImage);
    }

    @Override
    public void bindData(Integer data, int position, int pageSize) {
        ImageView itemImage = findView(R.id.itemImage);
        GlideApp.with(itemView).load(data).placeholder(R.drawable.ic_image_white_24dp).into(itemImage);
    }
}
