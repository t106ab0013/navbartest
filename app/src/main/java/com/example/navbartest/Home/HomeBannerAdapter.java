package com.example.navbartest.Home;

import android.view.View;

import com.example.navbartest.R;
import com.zhpan.bannerview.BaseBannerAdapter;

public class HomeBannerAdapter extends BaseBannerAdapter<Integer,HomeBannerImgHolder> {
    @Override
    protected void onBind(HomeBannerImgHolder holder, Integer data, int position, int pageSize) {
        holder.bindData(data, position, pageSize);
    }

    @Override
    public HomeBannerImgHolder createViewHolder(View itemView, int viewType) {
        return new HomeBannerImgHolder(itemView);
    }

    @Override
    public int getLayoutId(int viewType) {
        return R.layout.homebanner_image_layout;
    }
}
