package com.example.navbartest.Order;

public class Order {
    private String OrderID,ItemID,Owner,Buyer,StartDate,EndDate,Note;
    private long Period,Price,status;

    public Order(String OrderID,String ItemID,String Owner,String Buyer,String StartDate,String EndDate,String Note,long Period,long Price,long status){
        this.OrderID = OrderID;
        this.ItemID = ItemID;
        this.Owner = Owner;
        this.Buyer = Buyer;
        this.StartDate = StartDate;
        this.EndDate = EndDate;
        this.Note = Note;
        this.Period = Period;
        this.Price = Price;
        this.status = status;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getItemID() {
        return ItemID;
    }

    public void setItemID(String itemID) {
        ItemID = itemID;
    }

    public String getOwner() {
        return Owner;
    }

    public void setOwner(String owner) {
        Owner = owner;
    }

    public String getBuyer() {
        return Buyer;
    }

    public void setBuyer(String buyer) {
        Buyer = buyer;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public long getPeriod() {
        return Period;
    }

    public void setPeriod(int period) {
        Period = period;
    }

    public long getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
