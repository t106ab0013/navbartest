package com.example.navbartest.Order;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.navbartest.R;

public class OrderViewHolder extends RecyclerView.ViewHolder {

    public LinearLayout OrderItem;
    public ImageView OrderImage;
    public TextView tvOrderID,tvItemName,tvOrderDate,tvOrderStatus;

    public OrderViewHolder(View itemview){
        super(itemview);
        OrderItem = itemview.findViewById(R.id.OrderItem);
        OrderImage = itemview.findViewById(R.id.OrderImage);
        tvOrderID = itemview.findViewById(R.id.tvOrderID);
        tvItemName = itemview.findViewById(R.id.tvItemName);
        tvOrderDate = itemview.findViewById(R.id.tvOrderDate);
        tvOrderStatus = itemview.findViewById(R.id.tvOrderStatus);
    }

}
