package com.example.navbartest.Order;

import android.os.Parcelable;

public class Order_Status {
    public final static int STATUS_BSEND = 11000;
    public final static int STATUS_OREPLY = 11001;
    public final static int STATUS_ONRENT = 11002;
    public final static int STATUS_RETUNE = 11003;
    public final static int STATUS_COMPLETE = 11004;
    public final static int STATUS_BCANCEL = 11010;
    public final static int STATUS_OCANCEL = 11011;
    public Order_Status(String status){

    }

    public static String getStatusDes(long statusCode,int mode){
        /**
         * mode為1時顯示買家狀態
         *
         * 1.租借方送訂單出去
         * 11000
         * 2.買家同意 (不同意)->取消重新下單
         * 11001
         * 3.租借方確定收到
         * 11002
         * 4.賣方確認租借方歸還
         * 11003
         * 5.結束
         * 11004
         *
         * 2. ~ 4. 商品顯示租借中
         *
         */
        if (statusCode == STATUS_BSEND){
            if (mode == 1){
                return "租借訂單送出 正在等待賣家確認";
            }
            else{
                return "正在等待您確認訂單";
            }
        }
        else if (statusCode == STATUS_OREPLY){
            if (mode == 1){
                return "賣家已同意該筆交易";
            }
            else{
                return "您已同意該筆交易";
            }
        }
        else if (statusCode == STATUS_ONRENT){
            return "商品出借中";
        }
        else if (statusCode == STATUS_RETUNE){
            if (mode == 1){
                return "商品歸還 待賣家確認";
            }
            else{
                return "請確認租借方歸還的商品";
            }
        }
        else if (statusCode == STATUS_COMPLETE){
            return "訂單完成!";
        }
        else if(statusCode == STATUS_BCANCEL){
            if (mode == 1){
                return "您已取消訂單";
            }
            else{
                return "出租方已取消訂單";
            }

        }
        else if(statusCode == STATUS_OCANCEL){
            if (mode == 1){
                return "租借方已取消訂單";
            }
            else{
                return "您已取消訂單";
            }

        }
        else{
            return "錯誤! 請聯繫客服";
        }
    }
}
