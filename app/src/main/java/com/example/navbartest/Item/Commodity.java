package com.example.navbartest.Item;

public class Commodity {
    private String ItemID;
    private String ItemDes;
    private String ItemLocation;
    private String ItemName;
    private String ItemOwner;
    private String ItemPrice;
    private String PostDate;
    private String ImgCount;
    private boolean lend;

    public Commodity(String ItemID,String ItemDes,String ItemLocation,String ItemName,String ItemOwner,String ItemPrice,String PostDate,String ImgCount,boolean lend){
        this.ItemID = ItemID;
        this.ItemDes = ItemDes;
        this.ItemLocation = ItemLocation;
        this.ItemName = ItemName;
        this.ItemOwner = ItemOwner;
        this.ItemPrice = ItemPrice;
        this.PostDate = PostDate;
        this.ImgCount = ImgCount;
        this.lend = lend;
    }

    public void setItemID(String itemID) {
        ItemID = itemID;
    }
    public String getItemID() {
        return ItemID;
    }

    public void setItemDes(String ItemDes) {
        this.ItemDes = ItemDes;
    }
    public String getItemDes() {
        return ItemDes;
    }

    public void setItemLocation(String ItemLocation) {
        this.ItemLocation = ItemLocation;
    }
    public String getItemLocation() {
        return ItemLocation;
    }

    public void setItemName(String ItemName) {
        this.ItemName = ItemName;
    }
    public String getItemName() {
        return ItemName;
    }

    public void setItemOwner(String ItemOwner) {
        this.ItemOwner = ItemOwner;
    }
    public String getItemOwner() {
        return ItemOwner;
    }

    public void setItemPrice(String ItemPrice) {
        this.ItemPrice = ItemPrice;
    }
    public String getItemPrice() {
        return ItemPrice;
    }

    public void setPostDate(String PostDate) {
        this.PostDate = PostDate;
    }
    public String getPostDate() {
        return PostDate;
    }

    public void setImgCount(String imgCount) {
        ImgCount = imgCount;
    }
    public String getImgCount() {
        return ImgCount;
    }

    public void setLend(boolean lend) {
        this.lend = lend;
    }
    public boolean getlend(){
        return lend;
    }
}
