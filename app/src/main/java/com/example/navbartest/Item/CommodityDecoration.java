package com.example.navbartest.Item;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CommodityDecoration extends RecyclerView.ItemDecoration{

    int space,column;

     public CommodityDecoration(int space,int column){
         this.space=space;
         this.column=column;
     }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        int position = parent.getChildAdapterPosition(view);
        int spanSpace = space*(column+1)/column;
        int colIndex = position % column;
        outRect.top = space;
        outRect.bottom = space;
        outRect.left = space*(colIndex+1)-spanSpace*colIndex;
        outRect.right = spanSpace*(colIndex+1)-space*(colIndex+1);
        if (position>=column){
            outRect.top = space;
        }
    }
}
