package com.example.navbartest.Item;

import android.view.View;

import androidx.annotation.NonNull;

import com.example.navbartest.R;
import com.google.firebase.storage.StorageReference;
import com.zhpan.bannerview.BaseBannerAdapter;
import com.zhpan.bannerview.BaseViewHolder;

public class CommodityBannerAdapter extends BaseBannerAdapter<StorageReference,BannerImageHolder> {

    @Override
    protected void onBind(BannerImageHolder holder, StorageReference data, int position, int pageSize) {
        holder.bindData(data, position, pageSize);
    }

    @Override
    public BannerImageHolder createViewHolder(View itemView, int viewType) {
        return new BannerImageHolder(itemView);
    }

    @Override
    public int getLayoutId(int viewType) {
        return R.layout.commodity_image_layout;
    }
}
