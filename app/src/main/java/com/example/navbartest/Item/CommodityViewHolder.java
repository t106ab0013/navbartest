package com.example.navbartest.Item;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.navbartest.R;

public class CommodityViewHolder extends RecyclerView.ViewHolder {

    public LinearLayout OrderItem;
    public CardView CommodityView;
    public ImageView iVCommodityImage;
    public TextView tVCommondityName,tVCommondityPrice,tVPostDate;

    public CommodityViewHolder(View itemview){
        super(itemview);
        OrderItem = itemview.findViewById(R.id.OrderItem);
        CommodityView = itemview.findViewById(R.id.CommodityView);
        iVCommodityImage = itemView.findViewById(R.id.iVCommodityImage);
        tVCommondityName = itemView.findViewById(R.id.tVCommondityName);
        tVCommondityPrice = itemView.findViewById(R.id.tVCommondityPrice);
        tVPostDate = itemView.findViewById(R.id.tVPostDate);
    }

}
