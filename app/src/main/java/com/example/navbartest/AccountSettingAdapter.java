package com.example.navbartest;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.navbartest.Activity.AccountProfileActivity;
import com.example.navbartest.Activity.LoginActivity;
import com.example.navbartest.Activity.MyCommodityActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class AccountSettingAdapter extends RecyclerView.Adapter<AccountSettingAdapter.MyViewHolder> {



    List<AccountSettingListModel> aSList;
    Context context;
    FirebaseAuth mAuth;

    public AccountSettingAdapter(List<AccountSettingListModel> aSList, Context context) {
        this.aSList = aSList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.accountsetting_layout,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.iVsettingIcon.setImageResource(aSList.get(position).getImageicon());
        holder.tVsettingtext.setText(aSList.get(position).getSettingText());
        holder.iVnextImage.setImageResource(aSList.get(position).getImagenext());

    }

    @Override
    public int getItemCount() {
        return aSList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements DialogInterface.OnClickListener {

        ImageView iVsettingIcon;
        TextView tVsettingtext;
        ImageView iVnextImage;


        public MyViewHolder(@NonNull final View itemView) {
            super(itemView);

            iVsettingIcon = itemView.findViewById(R.id.iVsettingIcon);
            tVsettingtext = itemView.findViewById(R.id.tVsettingtext);
            iVnextImage = itemView.findViewById(R.id.iVnextImage);
            mAuth = FirebaseAuth.getInstance();
            final String userID = mAuth.getCurrentUser().getUid();
            final DatabaseReference clearToken = FirebaseDatabase.getInstance().getReference().child("AccountFile").child(userID).child("token");

            final AlertDialog.Builder AD = new AlertDialog.Builder(itemView.getContext());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FirebaseUser user = mAuth.getInstance().getCurrentUser();

                        switch (getAdapterPosition()){
                            case 0:
                                break;

                            case 1:
                                Intent intentMyItem = new Intent(itemView.getContext(), MyCommodityActivity.class);
                                itemView.getContext().startActivity(intentMyItem);
                                break;

                            case 2:
                                break;

                            case 3:
                                    Intent intentProfile = new Intent(itemView.getContext(), AccountProfileActivity.class);
                                    itemView.getContext().startActivity(intentProfile);
                                break;

                            case 4:
                                    String DeviceModel = Build.DEVICE;
                                    String AndroidVersion = Build.ID;
                                    String context = "使用者UID:"+userID+"\n設備型號: "+DeviceModel+"\nAndroid版本: "+AndroidVersion+"\n[請詳述遇到的問題，如果可以請附上截圖 感謝!]";
                                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                                    emailIntent.setData(Uri.parse("mailto:t106ab0013@ntut.org.tw"));
                                    emailIntent.putExtra(emailIntent.EXTRA_SUBJECT,"RUBY-錯誤與問題申告");
                                    emailIntent.putExtra(emailIntent.EXTRA_TEXT,context);
                                    itemView.getContext().startActivity(emailIntent);
                                break;

                            case 5:
                                    AD.setTitle("登出");
                                    AD.setMessage("確定要登出嗎?");
                                    AD.setIcon(R.drawable.ic_warning_black_24dp);
                                    AD.setPositiveButton("確定",new DialogInterface.OnClickListener(){
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            clearToken.removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    FirebaseAuth.getInstance().signOut();
                                                    Intent intentRefresh = new Intent(itemView.getContext(), LoginActivity.class);
                                                    itemView.getContext().startActivity(intentRefresh);
                                                    ((Activity)itemView.getContext()).finish();
                                                }
                                            });

                                        }
                                    });
                                    AD.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    });
                                    AD.show();
                                break;
                        }
                }
            });


        }

        //implements DialogInterface.OnClickListener 必須要有，不然會報錯 實際上沒用
        @Override
        public void onClick(DialogInterface dialog, int which) {

        }
    }



}
