package com.example.navbartest.Common;

import android.app.ProgressDialog;
import android.content.res.Resources;

public class Common {
    /**
     * 2020/07/16 新增
     * 很常用到的function可以寫在這 By紘瑜
     */

    public static int getScreenWidth(){
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight(){
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static int dp2px(int dp){
        int dpi = Resources.getSystem().getDisplayMetrics().densityDpi;
        return (dp*dpi)/160;
    }

    public static int px2dp(int px){
        int dpi = Resources.getSystem().getDisplayMetrics().densityDpi;
        return (px*160)/dpi;
    }

    /**
     * 2020-07-22 新增 By 紘瑜
     * 用這個方法前要先在自己的Activity裡面把ProgressDialog初始化 ex: loadingBar = new ProgressDialog(CommodityActivity.this);
     * 上面的是取消，直接用就會 多型導向過去下面的取消，可以少打2個參數
     * 在管理上也比較方便，如果分開寫在每個Activity要改會改到死
     */
    public static void setLoadingBar(ProgressDialog progressDialog) {
        //必須確定已經打開才能呼叫
        setLoadingBar(progressDialog," ",false);
    }
    public static void setLoadingBar(ProgressDialog progressDialog, String s, boolean status){
        //progressDialog = new ProgressDialog(context);
        if (status){
            progressDialog.setMessage(s);
            progressDialog.show();
            progressDialog.setCanceledOnTouchOutside(false);
        }
        else{
            progressDialog.dismiss();
        }
    }

}
