package com.example.navbartest.Common;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

public class CommonDialog extends AlertDialog.Builder {

    private DialogInterface.OnClickListener mPositiveListener = null;
    private DialogInterface.OnClickListener mNegativeListener = null;
    private DialogInterface.OnClickListener mNeutralListener = null;

    private CharSequence postext = null;
    private CharSequence negtext = null;
    private CharSequence neutext = null;


    public CommonDialog(Context context) {
        super(context);
    }

    @Override
    public AlertDialog.Builder setPositiveButton(CharSequence text, DialogInterface.OnClickListener listener) {
        mPositiveListener = listener;
        postext = text;
        return this;
    }

    @Override
    public AlertDialog.Builder setNegativeButton(CharSequence text, DialogInterface.OnClickListener listener) {
        mNegativeListener = listener;
        negtext = text;
        return this;
    }

    @Override
    public AlertDialog.Builder setNeutralButton(CharSequence text, DialogInterface.OnClickListener listener) {
        mNeutralListener = listener;
        neutext = text;
        return this;
    }



    @Override
    public AlertDialog show() {
        final AlertDialog alertDialog = super.create();
        DialogInterface.OnClickListener emptyOnClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        };

        if (postext!=null){
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,postext,emptyOnClickListener);
        }
        if (neutext!=null){
            alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL,neutext,emptyOnClickListener);
        }
        if (negtext!=null){
            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,negtext,emptyOnClickListener);
        }
            alertDialog.show();
        if (mPositiveListener != null){
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPositiveListener.onClick(alertDialog,AlertDialog.BUTTON_POSITIVE);
                }
            });
        }
        if (mNeutralListener != null){
            alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mNeutralListener.onClick(alertDialog,AlertDialog.BUTTON_NEUTRAL);
                }
            });
        }
        if (mNegativeListener != null){
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mNegativeListener.onClick(alertDialog,AlertDialog.BUTTON_NEGATIVE);
                }
            });
        }

        return alertDialog;
    }
}
