package com.example.navbartest.Common;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import androidx.appcompat.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.navbartest.R;

public class ToolbarAlphaBehavior extends CoordinatorLayout.Behavior<Toolbar> {
    private static final String TAG = "ToolbarAlphaBehavior";
    private int offset = 0;
    private int startOffset = 0;
    private int endOffset = 0;
    private Context context;

    /**
     *  控制toolbar在上滑時會逐漸透明
     *  在XML裡的屬性直接套就能用了
     */

    public ToolbarAlphaBehavior(Context context,AttributeSet attrs){
        super(context,attrs);
        this.context = context;
    }

    @Override
    public boolean onStartNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull Toolbar child, @NonNull View directTargetChild, @NonNull View target, int axes, int type) {
        return true;
    }

    @Override
    public void onNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull Toolbar toolbar, @NonNull View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed, int type, @NonNull int[] consumed) {
        startOffset = 0;
        endOffset = context.getResources().getDimensionPixelOffset(R.dimen.commodity_height) - toolbar.getHeight();
        offset += dyConsumed;
        if (offset<=startOffset){
            toolbar.getBackground().setAlpha(0);
        }
        else if(offset>startOffset && offset<endOffset){
            float percent = (float) (offset-startOffset)/endOffset;
            int alpha = Math.round(percent*255);
            toolbar.getBackground().setAlpha(alpha);
        }
        else if(offset>=endOffset){
            toolbar.getBackground().setAlpha(255);
        }
        Log.d("offset ", String.valueOf(offset));
    }

}
