package com.example.navbartest.Common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 2020/08/20新增
 * 日期 租借天數計算
 */
public class timeCal {
    public static int dayCal(String start,String end){
        Date dateStart = null,dateEnd = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            dateStart = format.parse(start);
            dateEnd = format.parse(end);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar fromcalender = Calendar.getInstance();
        fromcalender.setTime(dateStart);
        fromcalender.set(Calendar.HOUR_OF_DAY,0);
        fromcalender.set(Calendar.MINUTE,0);
        fromcalender.set(Calendar.SECOND,0);
        fromcalender.set(Calendar.MILLISECOND,0);

        Calendar tocalender = Calendar.getInstance();
        tocalender.setTime(dateEnd);
        tocalender.set(Calendar.HOUR_OF_DAY,0);
        tocalender.set(Calendar.MINUTE,0);
        tocalender.set(Calendar.SECOND,0);
        tocalender.set(Calendar.MILLISECOND,0);

        //回傳差幾天
        return (int) ((tocalender.getTime().getTime() - fromcalender.getTime().getTime()) / (1000 * 60 * 60 * 24));
    }
}
