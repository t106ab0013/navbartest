package com.example.navbartest.Common;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.navbartest.Order.Order;
import com.example.navbartest.Order.Order_Status;
import com.example.navbartest.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class DialogHelper {

    public static void simpleDialogNoListener(Activity activity,String title,String message,String posBtntext){
        new AlertDialog.Builder(activity)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(posBtntext, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

    public static void simpleDialog2Listener(Context context, String title, String message, String posBtntext, String nagBtntext
            , DialogInterface.OnClickListener posListener, DialogInterface.OnClickListener nagListener){
        CommonDialog dialog = new CommonDialog(context);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setPositiveButton(posBtntext,posListener);
        dialog.setNegativeButton(nagBtntext,nagListener);

        dialog.show();

    }

    public static void orderDialog(Activity activity, Order model, final int mode, String title, String posBtntext, String nagBtntext, String neuBtntext
            , DialogInterface.OnClickListener posListener, DialogInterface.OnClickListener nagListener, DialogInterface.OnClickListener neuListener){

        DatabaseReference mAccRef,userRef;
        FirebaseFirestore mStore = FirebaseFirestore.getInstance();
        DocumentReference order;

        String orderID = model.getOrderID();
        String itemID = model.getItemID();
        order = mStore.collection("Order").document(orderID);

        CommonDialog dialog = new CommonDialog(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        View v = inflater.inflate(R.layout.alertdialog_order_bsend,null);
        final TextView tvwho = v.findViewById(R.id.tvwho);
        final TextView tvtel = v.findViewById(R.id.tvtel);
        TextView tvperiod = v.findViewById(R.id.tvperiod);
        TextView tvprice = v.findViewById(R.id.tvprice);
        TextView tvnote = v.findViewById(R.id.tvnote);
        TextView tvstatus = v.findViewById(R.id.tvstatus);
        final TextView tvmessage = v.findViewById(R.id.tvmessage);
        tvperiod.setText("租借期間 : \n "+model.getStartDate()+" ~ "+model.getEndDate());
        tvprice.setText("訂單金額 : "+model.getPeriod()+" 天* "+model.getPrice()+"元，共 "+model.getPrice()*model.getPeriod()+" 元");
        tvnote.setText("租借者備註 : \n "+model.getNote());
        tvstatus.setText("訂單狀態 : "+ Order_Status.getStatusDes(model.getStatus(),mode));

        mAccRef = FirebaseDatabase.getInstance().getReference().child("AccountFile");

        /*設定聯絡資訊*/
        if (mode == 1){
            userRef = mAccRef.child(model.getOwner());
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot s) {
                    String[] conc = new String[2];
                    conc[0] = (String)s.child("username").getValue();
                    conc[1] = (String)s.child("userphone").getValue();
                    tvwho.setText("出租者姓名 : "+conc[0]);
                    tvtel.setText("連絡電話 : "+conc[1]);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
        else if (mode == 0){
            userRef = mAccRef.child(model.getBuyer());
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot s) {
                    String[] conc = new String[2];
                    conc[0] = (String)s.child("username").getValue();
                    conc[1] = (String)s.child("userphone").getValue();
                    tvwho.setText("租借者姓名 : "+conc[0]);
                    tvtel.setText("連絡電話 : "+conc[1]);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }

        int status = (int)model.getStatus();

        /*設定提示訊息內容*/
        switch (status){
            case Order_Status.STATUS_BSEND:
                tvmessage.setVisibility(View.GONE);
                break;
            case Order_Status.STATUS_OREPLY:
                if (mode == 1){
                    tvmessage.setText("*請於面交時與出租者當面確認物品狀況、內容是否與描述相符! 如有問題請立刻向出租者反應");
                }
                else{
                    tvmessage.setText("請於約定時間、地點與租借者面交出租商品。若有問題請聯繫租借者!");
                }
                break;
            case Order_Status.STATUS_ONRENT:
                if (mode == 1){
                    tvmessage.setText("*請依與出租者約定的時間、地點歸還物品 如有問題請立刻向出租者反應");
                }
                else{
                    tvmessage.setText("*請於約定時間、地點與租借者取回出租商品。若有問題請聯繫租借者!");
                }
                break;
            case Order_Status.STATUS_RETUNE:
                if (mode == 1){
                    tvmessage.setText("*請與出租者確認歸還的商品 確認無誤請出租方按下確認");
                }
                else{
                    tvmessage.setText("*請確認租借者歸還的商品 若確認無誤請按我已收到歸還商品");
                }
                break;
            case Order_Status.STATUS_COMPLETE:
                tvtel.setVisibility(View.GONE);
                order.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        String timeStamp = (String)task.getResult().get("completeTimeStamp");
                        tvmessage.setText("訂單已於 "+timeStamp+" 完成");
                    }
                });
                break;
            case Order_Status.STATUS_BCANCEL:
                tvtel.setVisibility(View.GONE);
                order.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        String timeStamp = (String)task.getResult().get("cancelTimeStamp");
                        if (mode == 1){
                            tvmessage.setText("*您已於 "+timeStamp+" 取消該筆訂單");
                        }
                        else {
                            tvmessage.setText("*對方已於 "+timeStamp+" 取消訂單");
                        }
                    }
                });
                break;
            case Order_Status.STATUS_OCANCEL:
                tvtel.setVisibility(View.GONE);
                order.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        String[] ocancel = new String[2];
                        ocancel[0] = (String)task.getResult().get("cancelTimeStamp");
                        ocancel[1] = (String)task.getResult().get("refuse");
                        if (mode == 1){
                            tvmessage.setText("出租方已於 "+ocancel[0]+" 取消"+"\n"+"不能出借的原因 : "+ocancel[1]);
                        }
                        else{
                            tvmessage.setText("您已於 "+ocancel[0]+" 取消"+"\n"+"不能出借的原因 : "+ocancel[1]);
                        }

                    }
                });
                break;
        }



        dialog.setTitle(title);
        dialog.setIcon(R.drawable.ic_order_icon_black_24dp);
        dialog.setView(v);
        dialog.setPositiveButton(posBtntext,posListener);
        dialog.setNeutralButton(neuBtntext,neuListener);
        dialog.setNegativeButton(nagBtntext,nagListener);
        dialog.show();

    }


}
