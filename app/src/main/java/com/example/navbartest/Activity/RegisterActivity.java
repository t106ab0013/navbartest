package com.example.navbartest.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.navbartest.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    ImageView iVAccountPhoto;
    Uri uri,uploadURI;
    Bitmap croppedImage;
    FirebaseAuth mAuth;
    FirebaseFirestore mStore;
    DatabaseReference mAccountRef;
    StorageReference mAccountImageRef;
    String userID;
    ProgressDialog loadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //進畫面不會自動跳鍵盤
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        mAuth = FirebaseAuth.getInstance();
        mStore = FirebaseFirestore.getInstance();

        final Button btRegister = (Button)findViewById(R.id.btRegister);

        final EditText etAccount = (EditText)findViewById(R.id.etAccount);
        final EditText etPassword = (EditText)findViewById(R.id.etPassword);
        final EditText etUserName = (EditText)findViewById(R.id.etUserName);
        final EditText etUserEmail = (EditText)findViewById(R.id.etUserEmail);
        final EditText etUserPhone = (EditText)findViewById(R.id.etUserPhone);
        final EditText etUserAddress = (EditText)findViewById(R.id.etUserAddress);
        final Spinner sptradeArea = (Spinner)findViewById(R.id.spArea_List);

        final String[] area = getResources().getStringArray(R.array.Area);

        loadingBar = new ProgressDialog(this);


        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String account = etAccount.getText().toString().trim();
                final String password = etPassword.getText().toString().trim();
                final String username = etUserName.getText().toString();
                final String useremail = etUserEmail.getText().toString().trim();
                final String userphone = etUserPhone.getText().toString().trim();
                final String useraddress = etUserAddress.getText().toString().trim();
                final int idarea = sptradeArea.getSelectedItemPosition();

                //用Firebase註冊及儲存使用者資料
                mAuth.createUserWithEmailAndPassword(useremail,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(RegisterActivity.this,"註冊成功 !",Toast.LENGTH_SHORT).show();
                            userID = mAuth.getCurrentUser().getUid();
                            mAccountRef = FirebaseDatabase.getInstance().getReference().child("AccountFile").child(userID);

                            loadingBar.setTitle("請稍後!!!");
                            loadingBar.setMessage("正在進行註冊...");
                            loadingBar.show();
                            loadingBar.setCanceledOnTouchOutside(true);

                            //設定資料內容
                            Map<String,Object> userfile = new HashMap<>();
                            userfile.put("AccountImageURL",null);
                            userfile.put("useremail",useremail);
                            userfile.put("username",username);
                            userfile.put("account",account);
                            userfile.put("password",password);
                            userfile.put("userphone",userphone);
                            userfile.put("useraddress",useraddress);
                            userfile.put("usertradeArea",area[idarea]);

                            //在Firebase的資料庫裡寫入資料
                            mAccountRef.setValue(userfile).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    Log.d("userfile SAVE", "onComplete: "+userID+" / "+useremail);
                                    Intent loginIntent = new Intent(RegisterActivity.this,LoginActivity.class);
                                    RegisterActivity.this.startActivity(loginIntent);
                                    finish();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.d("userfile SAVE","onFailure: "+e.toString());
                                }
                            });

                            /*   將資料新增到Storeage(非實時database)
                            DocumentReference documentReference = mStore.collection("AccountFile").document(userID);
                            documentReference.set(userfile).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    Log.d("userfile SAVE", "onComplete: "+userID+" / "+useremail);
                                    Intent loginIntent = new Intent(RegisterActivity.this,LoginActivity.class);
                                    RegisterActivity.this.startActivity(loginIntent);
                                    finish();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.d("userfile SAVE","onFailure: "+e.toString());
                                }
                            });*/

                        }
                        else{
                            Toast.makeText(RegisterActivity.this,"錯誤 !"+task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    }
                });


            }
        });

    }

}
