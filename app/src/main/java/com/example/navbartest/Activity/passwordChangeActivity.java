package com.example.navbartest.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.navbartest.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class passwordChangeActivity extends AppCompatActivity {
    FirebaseAuth mAuth;
    DatabaseReference mAccountRef;
    String userID;
    ProgressDialog loadingBar;
    Intent backtoMainActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_change);
        setTitle("密碼更改");
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        final EditText eTNewPassword = findViewById(R.id.eTNewPassword);
        final EditText eTConfirmPassword = findViewById(R.id.eTConfirmPassword);
        final TextView tVErrorMessage = findViewById(R.id.tVErrorMessage);
        final Button btConfirm = findViewById(R.id.btConfirm);

        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        mAccountRef = FirebaseDatabase.getInstance().getReference().child("AccountFile").child(userID);

        tVErrorMessage.setText("");


        btConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String temp1,temp2;
                temp1 = eTNewPassword.getText().toString().trim();
                temp2 = eTConfirmPassword.getText().toString().trim();
                final Map<String,Object> userfile = new HashMap<>();

                    if(temp1.length()>0 && temp2.length()>0){
                        if(temp1.equals(temp2)){
                            userfile.put("password",temp2);
                            mAuth.getCurrentUser().updatePassword(temp2).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        setLoadingBar();
                                        mAccountRef.updateChildren(userfile);
                                        Toast.makeText(passwordChangeActivity.this,"成功修改 !",Toast.LENGTH_SHORT).show();
                                        setBacktoMainActivity();
                                    }
                                    else{
                                        Log.d("change","FAil");
                                    }
                                }
                            });
                        }
                    }
                    else{
                        Toast.makeText(passwordChangeActivity.this,"密碼有誤 請再次確認",Toast.LENGTH_SHORT).show();
                        tVErrorMessage.setText("");
                    }

            }
        });

    }
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                new AlertDialog.Builder(passwordChangeActivity.this)
                        .setTitle("確定要返回嗎 ?")
                        .setMessage("資料會全部捨棄喔!!!")
                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setNeutralButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setLoadingBar(){
        loadingBar = new ProgressDialog(this);
        loadingBar.setMessage("正在修改資料...");
        loadingBar.show();
        loadingBar.setCanceledOnTouchOutside(true);
    }

    private void setBacktoMainActivity(){
        backtoMainActivity = new Intent(passwordChangeActivity.this,MainActivity.class);
        backtoMainActivity.putExtra("FragmentID",4);
        startActivity(backtoMainActivity);
    }

}
