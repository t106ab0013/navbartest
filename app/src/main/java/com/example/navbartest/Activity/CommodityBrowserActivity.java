package com.example.navbartest.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.navbartest.Common.Common;
import com.example.navbartest.Common.timeCal;
import com.example.navbartest.GlideApp;
import com.example.navbartest.Item.BannerImageHolder;
import com.example.navbartest.Item.CommodityBannerAdapter;
import com.example.navbartest.Order.Order_Status;
import com.example.navbartest.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.zhpan.bannerview.BannerViewPager;
import com.zhpan.bannerview.constants.IndicatorGravity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class CommodityBrowserActivity extends AppCompatActivity {
    Toolbar toolbar;
    ProgressDialog loadingBar;
    BannerViewPager<StorageReference, BannerImageHolder> banner;
    FirebaseFirestore mStore;
    DocumentReference order;
    FirebaseAuth mAuth;
    DatabaseReference mAccountRef,cartRef,itemRef;
    StorageReference itemImageRef;
    ArrayList<StorageReference> imglist = new ArrayList<>();
    String userID,ItemID,owner;
    int imgcount,price;
    boolean lendstatus;
    TextView tVitemname,tVitemprice,tVpostdate,tVitemdes,tvitemLocation,tVOwnerName,tvChat,tvAddCart,tVEndDate;
    LinearLayout btnChat,btnAddCart,btnOwner;
    Button btnRent;
    ImageView iVOwner,Chat,AddCart;
    ArrayList<String> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commodity_browser);
        toolbar = findViewById(R.id.toolbar);
        banner = findViewById(R.id.banner);

        tVitemname = findViewById(R.id.tVitemname);
        tVitemprice = findViewById(R.id.tVitemprice);
        tVpostdate = findViewById(R.id.tVpostdate);
        tVitemdes = findViewById(R.id.tVitemdes);
        tvitemLocation = findViewById(R.id.tvitemLocation);
        tVOwnerName = findViewById(R.id.tVOwnerName);
        tvChat = findViewById(R.id.tvChat);
        tvAddCart = findViewById(R.id.tvAddCart);
        tVEndDate = findViewById(R.id.tVEndDate);

        btnRent = findViewById(R.id.btnRent);
        btnChat = findViewById(R.id.btnChat);
        btnAddCart = findViewById(R.id.btnAddCart);
        btnOwner = findViewById(R.id.btnOwner);
        iVOwner = findViewById(R.id.iVOwner);
        Chat = findViewById(R.id.Chat);
        AddCart = findViewById(R.id.AddCart);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser()!=null){
            userID = mAuth.getCurrentUser().getUid();
        }
        mStore = FirebaseFirestore.getInstance();

        loadingBar = new ProgressDialog(CommodityBrowserActivity.this);
        list = new ArrayList<>();
        tVEndDate.setVisibility(View.GONE);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        ItemID = intent.getStringExtra("ItemID");
        lendstatus = intent.getBooleanExtra("lend",false);

        itemRef = FirebaseDatabase.getInstance().getReference().child("ItemFile").child(ItemID);
        setupData();
        itemImageRef = FirebaseStorage.getInstance().getReference().child("CommodityPhoto").child(ItemID);

        imgcount = Integer.parseInt(intent.getStringExtra("ImgCount"));

        for (int i=1;i<=imgcount;i++){
            imglist.add(itemImageRef.child(String.valueOf(i)));
        }
        setupbanner();

        if(intent.getStringExtra("ItemOwner").equals(userID)){
            preview();
        }
        else if (lendstatus){
            onlend();
        }


    }

    private void addShoppingCart(){
        cartRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot s) {
                List<String> cart = new ArrayList<>();
                /*購物車有東西的時候，先把裡面舊有商品抓下來、新增現在這個之後再回傳*/
                if (s.exists()){
                    for (DataSnapshot temp:s.getChildren()){
                        String IteminCart = (String)temp.getValue();
                        cart.add(IteminCart);
                    }
                }

                if (cart.contains(ItemID)){
                    Toast.makeText(CommodityBrowserActivity.this,"購物車內已有該商品",Toast.LENGTH_SHORT).show();
                }
                else{
                    HashMap<String,Object> reupload = new HashMap<>();
                    cart.add(ItemID);
                    reupload.put("cart",cart);
                    DatabaseReference reAdd = FirebaseDatabase.getInstance().getReference().child("AccountFile").child(userID);
                    reAdd.updateChildren(reupload).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(CommodityBrowserActivity.this,"成功加入購物車 !",Toast.LENGTH_SHORT).show();
                        }
                    });
                    btnAddCart.setBackground(ContextCompat.getDrawable(CommodityBrowserActivity.this,R.drawable.shape_background_2));
                    AddCart.setImageResource(R.drawable.ic_add_shopping_cart_white_24dp);
                    tvAddCart.setText("已加入購物車");
                    tvAddCart.setTextColor(ContextCompat.getColor(CommodityBrowserActivity.this,R.color.colorWhite));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void dateDialog(){
        LayoutInflater inflater = LayoutInflater.from(CommodityBrowserActivity.this);
        View view = inflater.inflate(R.layout.alertdialog_rentdate,null);
        final EditText etDateStart = view.findViewById(R.id.etDateStart);
        final EditText etDateEnd = view.findViewById(R.id.etDateEnd);
        final TextView tVDays = view.findViewById(R.id.tVDays);
        final EditText eTNote = view.findViewById(R.id.eTNote);
        final CheckBox checkBox_confirm = view.findViewById(R.id.checkBox_confirm);

        long datetemp = System.currentTimeMillis();
        SimpleDateFormat postdate = new SimpleDateFormat("yyyy-MM-dd");
        Date resultDate1 = new Date(datetemp-1000);
        Date resultDate2 = new Date(datetemp+86400000);
        etDateStart.setText(postdate.format(resultDate1));
        etDateEnd.setText(postdate.format(resultDate2));
        tVDays.setText("");

        etDateStart.setKeyListener(null);
        etDateEnd.setKeyListener(null);

        int days = timeCal.dayCal(etDateStart.getText().toString(),etDateEnd.getText().toString());
        tVDays.setText("租借期間共: "+days+" 天，總價為: "+days*price+" 元");

        etDateStart.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    datePicker(etDateStart);
                }
            }
        });
        etDateEnd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    datePicker(etDateEnd);
                }
            }
        });
        etDateEnd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int days = timeCal.dayCal(etDateStart.getText().toString(),etDateEnd.getText().toString());
                if (days<=0){
                    tVDays.setText("日期錯誤! 請重新設定");
                }else{
                    tVDays.setText("租借期間共: "+days+" 天，總價為: "+days*price+" 元");
                }
            }
        });

        new AlertDialog.Builder(CommodityBrowserActivity.this)
                .setView(view)
                .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (checkBox_confirm.isChecked()){
                            Common.setLoadingBar(loadingBar,"訂單發送中...",true);
                            int days = timeCal.dayCal(etDateStart.getText().toString(),etDateEnd.getText().toString());
                            long timestamp = System.currentTimeMillis();
                            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
                            HashMap<String,Object> data = new HashMap<>();
                            data.put("StartDate",etDateStart.getText().toString());
                            data.put("EndDate",etDateEnd.getText().toString());
                            data.put("Buyer",userID);
                            data.put("ItemID",ItemID);
                            data.put("Owner",owner);
                            data.put("Period",days);
                            data.put("Price",price);
                            data.put("Note",eTNote.getText().toString());
                            data.put("status",Order_Status.STATUS_BSEND);

                            order = mStore.collection("Order").document(String.valueOf(format.format(timestamp)));
                            order.set(data).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    new AlertDialog.Builder(CommodityBrowserActivity.this)
                                            .setTitle("訂單發送成功")
                                            .setMessage("請待出租者聯絡~")
                                            .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Common.setLoadingBar(loadingBar);
                                                }
                                            })
                                            .show();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.e("Order ERROR! ",e.toString());
                                }
                            });
                        }
                        else{
                            new AlertDialog.Builder(CommodityBrowserActivity.this)
                                    .setTitle("錯誤!")
                                    .setMessage("請確定日期與核取按鈕是否勾選")
                                    .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    })
                                    .show();
                        }
                    }
                })
                .setNeutralButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

    private void datePicker(final EditText e){
        SimpleDateFormat postdate = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(calendar.YEAR);
        int month = calendar.get(calendar.MONTH);
        int day = calendar.get(calendar.DAY_OF_MONTH);
        new DatePickerDialog(CommodityBrowserActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String datetime = String.format("%d-%02d-%02d",year,month+1,dayOfMonth);
                e.setText(datetime);
            }
        },year,month,day).show();
    }

    private void setupData(){
        itemRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot s) {
                owner = (String)s.child("ItemOwner").getValue();
                tVitemname.setText((String)s.child("ItemName").getValue());
                tVitemprice.setText("$ "+s.child("ItemPrice").getValue());
                price = Integer.valueOf((String)s.child("ItemPrice").getValue());
                tVpostdate.setText("刊登日期: "+s.child("PostDate").getValue());
                tVitemdes.setText((String)s.child("ItemDes").getValue());
                tvitemLocation.setText("商品位於: "+s.child("ItemLocation").getValue());

                /*2020-09-14 如果商品借出時，顯示到期日*/
                if (s.child("EndDate").exists() && lendstatus ==true){
                    String date = (String) s.child("EndDate").getValue();
                    tVEndDate.setVisibility(View.VISIBLE);
                    tVEndDate.setText("商品出借中，預計歸還日期: "+date);

                }
                else{
                    tVEndDate.setVisibility(View.GONE);
                }

                setupOwnerData();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void checkCart(){
        cartRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot s) {
                List<String> cart = new ArrayList<>();
                /*購物車有東西的時候，先把裡面舊有商品抓下來、新增現在這個之後再回傳*/
                if (s.exists()){
                    for (DataSnapshot temp:s.getChildren()){
                        String IteminCart = (String)temp.getValue();
                        cart.add(IteminCart);
                    }
                }

                if (cart.contains(ItemID)){
                    btnAddCart.setBackground(ContextCompat.getDrawable(CommodityBrowserActivity.this,R.drawable.shape_background_2));
                    AddCart.setImageResource(R.drawable.ic_add_shopping_cart_white_24dp);
                    tvAddCart.setText("已加入購物車");
                    tvAddCart.setTextColor(ContextCompat.getColor(CommodityBrowserActivity.this,R.color.colorWhite));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void setupOwnerData(){
        mAccountRef = FirebaseDatabase.getInstance().getReference().child("AccountFile").child(owner);
        mAccountRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String[] accdata = new String[2];
                accdata[0] = (String)snapshot.child("username").getValue();
                accdata[1] = (String)snapshot.child("AccountImageURL").getValue();

                if (accdata[1]!=null){
                    GlideApp.with(CommodityBrowserActivity.this).load(accdata[1]).placeholder(R.drawable.ic_image_black_24dp).into(iVOwner);
                }
                tVOwnerName.setText(accdata[0]);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void setupbanner(){
        banner.setAdapter(new CommodityBannerAdapter())
                .setAutoPlay(false)
                .setCanLoop(false)
                .setIndicatorStyle(0)
                .setIndicatorSliderColor(getResources().getColor(R.color.indicator_back),getResources().getColor(R.color.colorRegisterColumn))
                .setIndicatorGravity(IndicatorGravity.CENTER)
                .create(imglist);
    }

    private void onlend(){
        //暫時先不做預約功能
        btnRent.setEnabled(false);
        btnRent.setText("商品出借中...");
    }

    private void preview(){
        //預覽 標題、下方按鈕disable
        /*2020-9-9 自己的還是可以按 待處理*/
        btnRent.setEnabled(false);
        btnRent.setText("此為您刊登的商品");
        btnChat.setClickable(false);
        btnAddCart.setClickable(false);
        btnOwner.setClickable(false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_commodity,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        toolbar.getBackground().setAlpha(0);
        if (mAuth.getCurrentUser()!=null){
            userID = mAuth.getCurrentUser().getUid();
            cartRef = FirebaseDatabase.getInstance().getReference().child("AccountFile").child(userID).child("cart");
            checkCart();
        }

        btnRent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAuth.getCurrentUser()!=null){
                    dateDialog();
                }
                else {
                    Intent i = new Intent(CommodityBrowserActivity.this,LoginActivity.class);
                    startActivity(i);
                }

            }
        });

        btnAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAuth.getCurrentUser()==null){
                    Intent i = new Intent(CommodityBrowserActivity.this,LoginActivity.class);
                    startActivity(i);
                }
                else {
                    if (userID.equals(owner)){
                        Toast.makeText(CommodityBrowserActivity.this,"此為您的商品 !",Toast.LENGTH_SHORT).show();
                    }
                    else {
                        addShoppingCart();
                    }
                }
            }
        });

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        toolbar.getBackground().setAlpha(255);
    }

}
