package com.example.navbartest.Activity;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.navbartest.Common.Common;
import com.example.navbartest.GlideApp;
import com.example.navbartest.Item.Commodity;
import com.example.navbartest.Item.CommodityDecoration;
import com.example.navbartest.Item.CommodityViewHolder;
import com.example.navbartest.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class search_activity extends AppCompatActivity {
    private static final String TAG ="search_activity ";
    TextView tvEmpty;
    Query queryRef;
    DatabaseReference itemRef;
    RecyclerView schoolRecyclerView;
    StorageReference itemImageRef;
    String schoolName,itemName;
    FirebaseRecyclerAdapter CommodityAdapter;
    final int MODE_QUERY = 1,MODE_SEARCH = 2;

    @Override
    protected void onCreate(@NonNull Bundle saveInstanceState){
        //add
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_search);
        setTitle("搜尋結果");
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        tvEmpty = findViewById(R.id.tvEmpty);
        Log.d(TAG,"OnCreate");

        schoolRecyclerView = findViewById(R.id.schoolrecyclerView);
        schoolRecyclerView.setLayoutManager(new GridLayoutManager(this,2));
        schoolRecyclerView.addItemDecoration(new CommodityDecoration(Common.dp2px(5),2));

        itemRef = FirebaseDatabase.getInstance().getReference().child("ItemFile");
        Intent i = getIntent();
        handleIntent(i);
        schoolName = i.getStringExtra("schoolName");



    }

    private void handleIntent(Intent i){
        if (Intent.ACTION_SEARCH.equals(i.getAction())){
            itemName = i.getStringExtra(SearchManager.QUERY);
            query(MODE_QUERY,itemName);
        }
        else{
            schoolName = i.getStringExtra("schoolName");
            query(MODE_SEARCH,schoolName);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        CommodityAdapter.startListening();
        schoolRecyclerView.addOnChildAttachStateChangeListener(new RecyclerView.OnChildAttachStateChangeListener() {
            @Override
            public void onChildViewAttachedToWindow(@NonNull View view) {
                if (schoolRecyclerView.getAdapter().getItemCount()==0){
                    tvEmpty.setVisibility(View.VISIBLE);
                }
                else{
                    tvEmpty.setVisibility(View.GONE);
                }
            }

            @Override
            public void onChildViewDetachedFromWindow(@NonNull View view) {
                if (schoolRecyclerView.getAdapter().getItemCount()==0){
                    tvEmpty.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        CommodityAdapter.stopListening();
    }

    private void query(int mode,String name){
        if (mode == MODE_QUERY){
            queryRef = itemRef.orderByChild("ItemName").startAt(name).endAt(name+"\uf8ff");
        }
        else{
            queryRef = itemRef.orderByChild("ItemLocation").equalTo(name);
        }
        FirebaseRecyclerOptions<Commodity> options = new FirebaseRecyclerOptions.Builder<Commodity>()
                .setQuery(queryRef, new SnapshotParser<Commodity>() {
                    @NonNull
                    @Override
                    public Commodity parseSnapshot(@NonNull DataSnapshot snapshot) {
                        return new Commodity(snapshot.getKey()
                                ,(String)snapshot.child("ItemDes").getValue()
                                ,(String)snapshot.child("ItemLocation").getValue()
                                ,(String)snapshot.child("ItemName").getValue()
                                ,(String)snapshot.child("ItemOwner").getValue()
                                ,(String)snapshot.child("ItemPrice").getValue()
                                ,(String)snapshot.child("PostDate").getValue()
                                ,(String)snapshot.child("ImgCount").getValue()
                                ,(boolean)snapshot.child("lend").getValue());

                    }
                })
                .build();
        CommodityAdapter = new FirebaseRecyclerAdapter<Commodity, CommodityViewHolder>(options) {

            @NonNull
            @Override
            public CommodityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.commodity_layout,parent,false);
                return new CommodityViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull CommodityViewHolder holder, int position, @NonNull final Commodity model) {
                ViewGroup.LayoutParams para = holder.iVCommodityImage.getLayoutParams();
                para.width = (Common.getScreenWidth()-Common.dp2px(20))/2;
                para.height = (Common.getScreenWidth()-Common.dp2px(20))/2;
                holder.iVCommodityImage.setLayoutParams(para);

                itemImageRef = FirebaseStorage.getInstance().getReference().child("CommodityPhoto").child(model.getItemID()).child("1");
                GlideApp.with(search_activity.this).load(itemImageRef).centerInside().into(holder.iVCommodityImage);
                holder.tVCommondityName.setText(model.getItemName());
                holder.tVCommondityPrice.setText("$ "+model.getItemPrice());
                if (model.getlend()){
                    holder.tVPostDate.setText("商品出借中");
                }
                else{
                    holder.tVPostDate.setText(model.getPostDate());
                }

                holder.CommodityView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(search_activity.this,CommodityBrowserActivity.class);
                        intent.putExtra("preview",false);
                        intent.putExtra("ItemID",model.getItemID());
                        intent.putExtra("ItemDes",model.getItemDes());
                        intent.putExtra("ItemLocation",model.getItemLocation());
                        intent.putExtra("ItemName",model.getItemName());
                        intent.putExtra("ItemOwner",model.getItemOwner());
                        intent.putExtra("ItemPrice",model.getItemPrice());
                        intent.putExtra("ImgCount",model.getImgCount());
                        intent.putExtra("PostDate",model.getPostDate());
                        intent.putExtra("lend",model.getlend());
                        startActivity(intent);
                    }
                });

            }

            @Override
            public int getItemCount() {
                return super.getItemCount();
            }

            @NonNull
            @Override
            public Commodity getItem(int position) {
                return super.getItem(position);
            }

            @Override
            public void onError(@NonNull DatabaseError error) {
                super.onError(error);
                Log.e("DataBase GET ", error.toString());
            }
        };
        schoolRecyclerView.setAdapter(CommodityAdapter);
    }

}
