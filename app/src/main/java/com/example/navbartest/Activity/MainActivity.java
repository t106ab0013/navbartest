package com.example.navbartest.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.bumptech.glide.Glide;
import com.example.navbartest.Fragment.AccountFragment;
import com.example.navbartest.Fragment.HomeFragment;
import com.example.navbartest.Fragment.NotificationsFragment;
import com.example.navbartest.Fragment.SchoolFragment;
import com.example.navbartest.Fragment.ShoppingCartFragment;
import com.example.navbartest.Item.Commodity;
import com.example.navbartest.Order.Order_Status;
import com.example.navbartest.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    BottomNavigationView btnavViewId;
    FirebaseFirestore mStore;
    FrameLayout frameLayoutid;
    FirebaseAuth mAuth;
    int nowFragmentID, nextFragmentID,unReadCount;
    FirebaseUser user;
    String userID,token;
    BadgeDrawable unreadBadge;
    Query queryRef;

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnavViewId = findViewById(R.id.bottomNavViewId);
        frameLayoutid = findViewById(R.id.frameLayoutId);

        mStore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getInstance().getCurrentUser();

        getToken();
        if (user!=null){
            userID = mAuth.getCurrentUser().getUid();
            unReadCount = 0;
            checkEndDate();
        }

        final int permissionWRITE = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        //確認權限狀況
        if (permissionWRITE != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }



        //設定其他Activity切換回來跳到哪個分頁
        int fragmentID = getIntent().getIntExtra("FragmentID", 0);
        switch (fragmentID) {
            case 0:
                btnavViewId.setSelectedItemId(R.id.homepage);
                setFragment(new HomeFragment());
                break;
            case 1:
                btnavViewId.setSelectedItemId(R.id.schoolpage);
                setFragment(new SchoolFragment());
                break;
            case 2:
                btnavViewId.setSelectedItemId(R.id.notificationspage);
                setFragment(new NotificationsFragment());
                break;
            case 3:
                btnavViewId.setSelectedItemId(R.id.shoppingcarpage);
                setFragment(new ShoppingCartFragment());
                break;
            case 4:
                btnavViewId.setSelectedItemId(R.id.accountpage);
                setFragment(new AccountFragment());
                break;
        }
        //點到分頁按鈕時切換到該分頁
        btnavViewId.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuitem) {
                nextFragmentID = menuitem.getItemId();
                /*-----2020/07/12修正 重複切換頁面不會閃退-----*/
                if (nowFragmentID == nextFragmentID) {
                    return false;
                } else {
                    switch (menuitem.getItemId()) {
                        case R.id.homepage:
                            setFragment(new HomeFragment());
                            nowFragmentID = R.id.homepage;
                            return true;
                        case R.id.schoolpage:
                            setFragment(new SchoolFragment());
                            nowFragmentID = R.id.schoolpage;
                            return true;
                        case R.id.notificationspage:
                            setFragment(new NotificationsFragment());
                            nowFragmentID = R.id.notificationspage;
                            return true;
                        case R.id.shoppingcarpage:
                            setFragment(new ShoppingCartFragment());
                            nowFragmentID = R.id.shoppingcarpage;
                            return true;
                        case R.id.accountpage:
                            if (user == null) {

                            } else {
                                setFragment(new AccountFragment());
                                nowFragmentID = R.id.accountpage;
                            }
                            return true;

                        default:
                            return false;
                    }
                }
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main,menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) MainActivity.this.getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(MainActivity.this.getComponentName()));
        }

        return super.onCreateOptionsMenu(menu);
    }



    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
           case R.id.chat:
               if (user == null){
                   Intent gotoLoginActivity = new Intent(MainActivity.this, LoginActivity.class);
                   startActivity(gotoLoginActivity);
               }
               else{
                   Intent i = new Intent(MainActivity.this,ChatListActivity.class);
                   startActivity(i);
               }
               break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameLayoutId, fragment);
        fragmentTransaction.commit();

    }

    private void getToken() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        final String deviceToken = instanceIdResult.getToken();
                        token = instanceIdResult.getToken();
                        Log.d("Token","token: "+token);
                        //如果有登入的時候確認Token
                        if (user!=null){
                            userID = mAuth.getCurrentUser().getUid();
                            DatabaseReference tokenRef = FirebaseDatabase.getInstance().getReference().child("AccountFile").child(userID).child("token");
                            final DatabaseReference update = FirebaseDatabase.getInstance().getReference().child("AccountFile").child(userID);
                            tokenRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot s) {
                                    if (s.exists()){
                                        String temp = (String)s.getValue();
                                        if (!temp.equals(token)){
                                            HashMap<String,Object> data = new HashMap<>();
                                            data.put("token",token);
                                            update.updateChildren(data).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    Toast.makeText(MainActivity.this,"資料更新成功...",Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }
                                    }
                                    else{
                                        HashMap<String,Object> data = new HashMap<>();
                                        data.put("token",token);
                                        update.updateChildren(data).addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Toast.makeText(MainActivity.this,"資料更新成功...",Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });
                        }
                    }
                });

            }
        }, 1000);
    }

    private void checkEndDate(){
        long datetemp = System.currentTimeMillis();
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        Date resultDate = new Date(datetemp+86400000);
        final String tomorrow = date.format(resultDate);
        Log.d("Date ",tomorrow);
        queryRef = mStore.collection("Order").whereEqualTo("Buyer",userID);
        queryRef.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                for (QueryDocumentSnapshot doc:value){
                    Log.d("Date ",doc.get("status").toString());
                    if (doc.get("EndDate").equals(tomorrow) && doc.get("status").equals((long)Order_Status.STATUS_ONRENT)){
                        unReadCount++;
                    }
                }
                addBagde(unReadCount);
            }
        });

    }

    private void addBagde(int count){
        unreadBadge = btnavViewId.getOrCreateBadge(R.id.notificationspage);
        if (count>0){
            unreadBadge.setNumber(count);
            unreadBadge.isVisible();
        }
        else{
            unreadBadge.setVisible(false);
        }
    }

}
