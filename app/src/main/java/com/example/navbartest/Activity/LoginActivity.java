package com.example.navbartest.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.navbartest.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    Button btLogin;
    TextView tvRegister;
    FirebaseAuth mAuth;
    ProgressDialog loadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btLogin = (Button)findViewById(R.id.btLogin);
        tvRegister = (TextView)findViewById(R.id.tvRegister);
        mAuth = FirebaseAuth.getInstance();

        final EditText etEmail = (EditText)findViewById(R.id.etEmail);
        final EditText etPassword = (EditText)findViewById(R.id.etPassword);



        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String email = etEmail.getText().toString().trim();
                final String password = etPassword.getText().toString().trim();

                mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            setLoadingBar();
                            Toast.makeText(LoginActivity.this,"登入成功 !",Toast.LENGTH_SHORT).show();
                            Intent accountFragment = new Intent(LoginActivity.this,MainActivity.class);
                            LoginActivity.this.startActivity(accountFragment);
                            finish();
                        }
                        else{
                            Toast.makeText(LoginActivity.this,"錯誤 !\n"+task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        });

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(LoginActivity.this,RegisterActivity.class);
                LoginActivity.this.startActivity(registerIntent);
            }
        });


    }

    private void setLoadingBar(){
        loadingBar = new ProgressDialog(this);
        loadingBar.setTitle("請稍後!!!");
        loadingBar.setMessage("登入中...");
        loadingBar.setCanceledOnTouchOutside(true);
        loadingBar.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)){
            Intent i = new Intent(LoginActivity.this,MainActivity.class);
            i.putExtra("FragmentID",0);
            startActivity(i);
        }
        return super.onKeyDown(keyCode, event);
    }
}
