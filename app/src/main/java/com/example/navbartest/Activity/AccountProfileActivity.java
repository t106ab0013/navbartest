package com.example.navbartest.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.navbartest.Common.Common;
import com.example.navbartest.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

public class AccountProfileActivity extends AppCompatActivity {

    ImageView iVAccountPhoto ;
    Uri uri,uploadURI;
    Bitmap croppedImage;
    FirebaseAuth mAuth;
    FirebaseFirestore mStore;
    DatabaseReference mAccountRef;
    StorageReference mAccountImageRef;
    String userID,tempPassword;
    ProgressDialog loadingBar;
    Intent backtoMainActivity;
    TextView tVname,tVaccount,tVphone,tVaddress,tVarea,tVpassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_profile);
        //設定最上方那欄的文字 & 返回建
        setTitle("帳戶資料設定");
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        mAccountRef = FirebaseDatabase.getInstance().getReference().child("AccountFile").child(userID);
        mAccountImageRef = FirebaseStorage.getInstance().getReference().child("AccountPhoto");
        iVAccountPhoto = findViewById(R.id.iVAccountPhoto);
        loadingBar = new ProgressDialog(AccountProfileActivity.this);


        final TableRow tRname = (TableRow) findViewById(R.id.tRname);
        TableRow tRaccount = (TableRow)findViewById(R.id.tRaccount);
        TableRow tRphone = (TableRow)findViewById(R.id.tRphone);
        TableRow tRaddress = (TableRow)findViewById(R.id.tRaddress);
        TableRow tRarea = (TableRow)findViewById(R.id.tRarea);
        TableRow tRchangePassword = (TableRow)findViewById(R.id.tRchangePassword);

        final TextView tVname = findViewById(R.id.tVname);
        final TextView tVaccount = findViewById(R.id.tVaccount);
        final TextView tVphone = findViewById(R.id.tVphone);
        final TextView tVaddress = findViewById(R.id.tVaddress);
        final TextView tVarea = findViewById(R.id.tVarea);
        final TextView tVpassword = findViewById(R.id.tVpassword);

        //載入使用者資料
        mAccountRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String[] userdata = new String[8];
                userdata[0] = (String)dataSnapshot.child("AccountImageURL").getValue();
                userdata[1] = (String)dataSnapshot.child("account").getValue();
                userdata[2] = (String)dataSnapshot.child("password").getValue();
                userdata[3] = (String)dataSnapshot.child("useraddress").getValue();
                userdata[4] = (String)dataSnapshot.child("useremail").getValue();
                userdata[5] = (String)dataSnapshot.child("username").getValue();
                userdata[6] = (String)dataSnapshot.child("userphone").getValue();
                userdata[7] = (String)dataSnapshot.child("usertradeArea").getValue();
                tempPassword = userdata[2];

                if(userdata[0]!=null){
                    Glide.with(AccountProfileActivity.this).load(userdata[0]).into(iVAccountPhoto);
                }
                tVname.setText(userdata[5]);
                tVaccount.setText(userdata[1]);
                tVphone.setText(userdata[6]);
                tVaddress.setText(userdata[3]);
                tVarea.setText(userdata[7]);
                tVpassword.setText(userdata[2].replaceAll("[a-zA-Z0-9]","*"));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //切換到選擇圖片的畫面
        iVAccountPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.startPickImageActivity(AccountProfileActivity.this);
            }
        });

        //點擊事件
        tRname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUserData(1);
            }
        });
        tRaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUserData(2);
            }
        });
        tRphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUserData(3);
            }
        });
        tRaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUserData(4);
            }
        });
        tRarea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUserData(5);
            }
        });
        tRchangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUserData(6);
            }
        });

    }

    //把畫面切出去到選圖片裁切的畫面 & 上傳圖片到儲存空間
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE
                && resultCode == Activity.RESULT_OK){
            Uri imageuri = CropImage.getPickImageResultUri(this,data);
            if(CropImage.isReadExternalStoragePermissionsRequired(this,imageuri)){
                uri = imageuri;
                requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},0);
            }
            else{
                StartCrop(imageuri);
            }
        }
        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if(resultCode == RESULT_OK){
                iVAccountPhoto.setImageURI(result.getUri());
                uploadURI = result.getUri();
                iVAccountPhoto.setDrawingCacheEnabled(true);
                croppedImage = Bitmap.createBitmap(iVAccountPhoto.getDrawingCache());
                iVAccountPhoto.setDrawingCacheEnabled(false);

                //將上傳圖片的步驟從按下按鈕改為選完圖片後馬上上傳
                final StorageReference filepath = mAccountImageRef.child(userID+".jpg");

                filepath.putFile(uploadURI).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if(task.isSuccessful()){
                            task.getResult().getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    final String downLoadURL = uri.toString();
                                    Log.d("URL", "onComplete: "+downLoadURL);
                                    mAccountRef.child("AccountImageURL").setValue(downLoadURL).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){
                                                Toast.makeText(AccountProfileActivity.this,"修改成功",Toast.LENGTH_SHORT).show();
                                            }
                                            else{
                                                String message = task.getException().getMessage();
                                                Toast.makeText(AccountProfileActivity.this,"錯誤 !"+message,Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                }
                            });
                        }
                        else{
                            Toast.makeText(AccountProfileActivity.this,"圖片裁切成功，但無法上傳",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }
    }

    //裁切圖片
    private void StartCrop(Uri imageuri) {
        CropImage.activity(imageuri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .setMaxCropResultSize(1080,1080)
                .setRequestedSize(320 ,320)
                .setAspectRatio(1,1)
                .start(this);
    }

    //儲存裁切完的圖片
    private void SaveCroppedImage(Bitmap bitmap,String image_name){
        // "/storage/emulated/0/Android/data/com.example.navbartest/files"
        String root = getExternalFilesDir("").getAbsolutePath()+"/";
        File myDir = new File(root);
        myDir.mkdirs();
        String fname = "AccountPhoto"+image_name+" .jpg";
        File file = new File(myDir,fname);
        if(file.exists()){
            file.delete();
        }
        Log.i("LOAD",root + fname);
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //***設定自訂工具列
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_checked,menu);
        return super.onCreateOptionsMenu(menu);
    }

    //上方工具列的點擊事件監聽
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                new AlertDialog.Builder(AccountProfileActivity.this)
                        .setTitle("確定要返回嗎 ?")
                        .setMessage("除了已上傳的圖片外其他不會儲存喔!!!")
                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setNeutralButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
                break;
            case R.id.toolBar_confirm_button:
                Common.setLoadingBar(loadingBar,"正在修改資料...",true);
                getUserSetData();
                SaveCroppedImage(croppedImage,userID);
                Common.setLoadingBar(loadingBar);
                setBacktoMainActivity();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //切換回MainActivity 並帶上參數
    private void setBacktoMainActivity(){
        backtoMainActivity = new Intent(AccountProfileActivity.this,MainActivity.class);
        backtoMainActivity.putExtra("FragmentID",4);
        startActivity(backtoMainActivity);
    }

    //取得與更新使用者資料
    public void getUserSetData(){
        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        mAccountRef = FirebaseDatabase.getInstance().getReference().child("AccountFile").child(userID);
        Map<String,Object> userfile = new HashMap<>();
        final TextView tVname = findViewById(R.id.tVname);
        final TextView tVaccount = findViewById(R.id.tVaccount);
        final TextView tVphone = findViewById(R.id.tVphone);
        final TextView tVaddress = findViewById(R.id.tVaddress);
        final TextView tVarea = findViewById(R.id.tVarea);
        final TextView tVpassword = findViewById(R.id.tVpassword);

        userfile.put("username",tVname.getText().toString());
        userfile.put("account",tVaccount.getText().toString().trim());
        userfile.put("userphone",tVphone.getText().toString().trim());
        userfile.put("useraddress",tVaddress.getText().toString().trim());
        userfile.put("usertradeArea",tVarea.getText().toString().trim());
        //更新資料
        mAccountRef.updateChildren(userfile);
    }

    //設定資料
    public void setUserData(int index){
        final TextView tVname = findViewById(R.id.tVname);
        final TextView tVaccount = findViewById(R.id.tVaccount);
        final TextView tVphone = findViewById(R.id.tVphone);
        final TextView tVaddress = findViewById(R.id.tVaddress);
        final TextView tVarea = findViewById(R.id.tVarea);
        final TextView tVpassword = findViewById(R.id.tVpassword);

        LayoutInflater inflater = LayoutInflater.from(AccountProfileActivity.this);
        View view = inflater.inflate(R.layout.alertdialog_input,null);
        TextView tVOlddata = view.findViewById(R.id.tVOlddata);
        TextView tVNewData = view.findViewById(R.id.tVNewData);
        final EditText eTNewdata = view.findViewById(R.id.eTNewdata);

        /**
         * 2020-07-16 新增array資源檔
         * 資源檔內新增縣市array並引用 by紘瑜
         */
        final String[] area = getResources().getStringArray(R.array.Area);

        switch (index){
            case 1:
                tVOlddata.setText("舊資料: "+tVname.getText());
                new AlertDialog.Builder(AccountProfileActivity.this)
                        .setTitle("更改資料")
                        .setView(view)
                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(eTNewdata.getText().toString().trim().equals("")){
                                    Toast.makeText(AccountProfileActivity.this,"更改失敗! 輸入不能為空值",Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    tVname.setText(eTNewdata.getText().toString());
                                }
                            }
                        })
                        .show();
                break;

            case 2:
                tVOlddata.setText("舊資料: "+tVaccount.getText());
                new AlertDialog.Builder(AccountProfileActivity.this)
                        .setTitle("更改資料")
                        .setView(view)
                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(eTNewdata.getText().toString().trim().equals("")){
                                    Toast.makeText(AccountProfileActivity.this,"更改失敗! 輸入不能為空值",Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    tVaccount.setText(eTNewdata.getText().toString());
                                }
                            }
                        })
                        .show();
                break;

            case 3:
                tVOlddata.setText("舊資料: "+tVphone.getText());
                eTNewdata.setInputType(InputType.TYPE_CLASS_PHONE);
                new AlertDialog.Builder(AccountProfileActivity.this)
                        .setTitle("更改資料")
                        .setView(view)
                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(eTNewdata.getText().toString().trim().equals("")){
                                    Toast.makeText(AccountProfileActivity.this,"更改失敗! 輸入不能為空值",Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    tVphone.setText(eTNewdata.getText().toString());
                                }
                            }
                        })
                        .show();
                break;

            case 4:
                tVOlddata.setText("舊資料: "+tVaddress.getText());
                new AlertDialog.Builder(AccountProfileActivity.this)
                        .setTitle("更改資料")
                        .setView(view)
                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(eTNewdata.getText().toString().trim().equals("")){
                                    Toast.makeText(AccountProfileActivity.this,"更改失敗! 輸入不能為空值",Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    tVaddress.setText(eTNewdata.getText().toString());
                                }
                            }
                        })
                        .show();
                break;

            case 5:
                AlertDialog.Builder areaList = new AlertDialog.Builder(AccountProfileActivity.this);
                areaList.setTitle("請選擇偏好交易的地區");
                areaList.setItems(area, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(AccountProfileActivity.this,"已選擇: "+area[which],Toast.LENGTH_SHORT).show();
                        tVarea.setText(area[which]);
                    }
                });
                areaList.show();
                break;

            case 6:
                eTNewdata.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                tVOlddata.setText("請輸入目前密碼");
                tVNewData.setText("目前密碼:");
                new AlertDialog.Builder(AccountProfileActivity.this)
                        .setTitle("更改密碼")
                        .setView(view)
                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(eTNewdata.getText().toString().trim().equals(tempPassword)){
                                    Intent topasswordpage = new Intent(AccountProfileActivity.this,passwordChangeActivity.class);
                                    startActivity(topasswordpage);
                                }
                                else{
                                    Toast.makeText(AccountProfileActivity.this,"密碼錯誤!",Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .show();

                break;

        }
    }

}
