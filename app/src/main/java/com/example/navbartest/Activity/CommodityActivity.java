package com.example.navbartest.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.MemoryCategory;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.donkingliang.imageselector.utils.ImageSelector;
import com.example.navbartest.Common.Common;
import com.example.navbartest.GlideApp;
import com.example.navbartest.Item.Commodity;
import com.example.navbartest.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class CommodityActivity extends AppCompatActivity {

    FirebaseAuth mAuth;
    StorageReference mCommodityImageRef,imageUpLoad,imagetest;
    DatabaseReference  mItem,mItemRef;
    ProgressDialog loadingBar;
    TextInputLayout etItemName,etItemPrice;
    TextView tvItemLocation;
    EditText etItemDes;
    String userID,itemID,date;
    ImageView imageView9;
    Button btnChoosePicture;
    TableRow trItemLocation;
    LinearLayout gallery;
    boolean edit_status,image_edit;
    int count;

    //確認執行的值
    private static final int CHECK_SUCCESS = 1000,CHECK_FAIL = 1001;


    ArrayList<String> selected = new ArrayList<>();
    int REQUEST_CODE = 2;
    ArrayList<Uri> uris = new ArrayList<Uri>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commodity);
        setTitle("刊登新商品");
        Log.d("THREAD ", String.valueOf(Thread.currentThread().getId()));
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();

        tvItemLocation = findViewById(R.id.tvItemLocation);
        etItemName = findViewById(R.id.etItemName);
        etItemPrice = findViewById(R.id.etItemPrice);
        etItemDes = findViewById(R.id.etItemDes);
        btnChoosePicture = findViewById(R.id.btn_ChoosePicture);
        trItemLocation = findViewById(R.id.trItemLocation);
        imageView9 = findViewById(R.id.imageView9);
        loadingBar = new ProgressDialog(CommodityActivity.this);
        gallery = findViewById(R.id.gallery);
        Intent intent = getIntent();
        image_edit = false;
        uris.clear();

        //判斷是否是修改
        edit_status = intent.getBooleanExtra("Edit_status",false);
        if(edit_status){
            setTitle("商品內容修改");
            itemID = intent.getStringExtra("ItemID");
            count = Integer.parseInt(intent.getStringExtra("ImgCount"));
            date = intent.getStringExtra("PostDate");
            mCommodityImageRef = FirebaseStorage.getInstance().getReference().child("CommodityPhoto").child(itemID);
            setItemData(intent);
        }

        /**
         * 2020-07-22 用Glide載入Firebase Storage儲存路徑的圖片
         */
        /*GlideApp.get(CommodityActivity.this).setMemoryCategory(MemoryCategory.HIGH);
        imagetest = FirebaseStorage.getInstance().getReference().child("CommodityPhoto").child("1595343164499").child("1");
        GlideApp.with(CommodityActivity.this).load(imagetest)
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        Log.d("ERR ",e.toString());
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imageView9);*/


        trItemLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemLocation();
            }
        });

        btnChoosePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uris.clear();
                image_edit = true;
                gallery = findViewById(R.id.gallery);
                gallery.removeAllViews();
                ImageSelector.builder()
                        .useCamera(true)// 设置是否使用拍照
                        .setSingle(false)  //设置是否单选
                        .setMaxSelectCount(9) // 图片的最大选择数量，小于等于0时，不限数量。
                        .setSelected(selected) // 把已选的图片传入默认选中。
                        .canPreview(true) //是否可以预览图片，默认为true
                        .start(CommodityActivity.this, REQUEST_CODE); // 打开相册
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (edit_status){
            getMenuInflater().inflate(R.menu.menu_edit,menu);
        }
        else{
            getMenuInflater().inflate(R.menu.menu_checked,menu);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                new AlertDialog.Builder(CommodityActivity.this)
                        .setTitle("確定要返回嗎 ?")
                        .setMessage("內容會全部捨棄喔!!!")
                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                goback();
                            }
                        })
                        .setNeutralButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
                break;
            case R.id.toolBar_confirm_button:
                Common.setLoadingBar(loadingBar,"上傳商品中...",true);
                if (getItemData() == CHECK_SUCCESS){
                    ImageUpLoad();
                }
                else{
                    new AlertDialog.Builder(CommodityActivity.this)
                            .setTitle("資料有誤")
                            .setMessage("內容、圖片皆不能為空值喔!")
                            .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Common.setLoadingBar(loadingBar);
                                }
                            })
                            .show();
                }
                break;
            case R.id.toolBar_edit_button:
                Common.setLoadingBar(loadingBar,"上傳商品中...",true);
                if (getItemData() == CHECK_SUCCESS){
                    if (image_edit){
                        for(int i=1;i<=count;i++){
                            mCommodityImageRef.child(String.valueOf(i)).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.d("DELETE ","SUCCESS!");
                                }
                            });
                        }
                        ImageUpLoad();
                    }
                }
                else {
                    new AlertDialog.Builder(CommodityActivity.this)
                            .setTitle("資料有誤")
                            .setMessage("內容、圖片皆不能為空值喔!")
                            .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Common.setLoadingBar(loadingBar);
                                }
                            })
                            .show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void itemLocation(){
        final String[] location = getResources().getStringArray(R.array.School);
        AlertDialog.Builder areaList = new AlertDialog.Builder(CommodityActivity.this);
        areaList.setTitle("請選擇偏好交易的地區");
        areaList.setItems(location, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(CommodityActivity.this,"已選擇: "+location[which],Toast.LENGTH_SHORT).show();
                tvItemLocation.setText(location[which]);
            }
        });
        areaList.show();
    }

    public void setItemData(Intent intent){
        //只有修改的時候才執行
        etItemName.getEditText().setText(intent.getStringExtra("ItemName"));
        etItemPrice.getEditText().setText(intent.getStringExtra("ItemPrice"));
        etItemDes.setText(intent.getStringExtra("ItemDes"));
        tvItemLocation.setText(intent.getStringExtra("ItemLocation"));
        //圖片展示的部分
        gallery.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(CommodityActivity.this);
        for(int i=1;i<=count;i++){
            View view =inflater.inflate(R.layout.item,gallery,false);
            ImageView imageView = view.findViewById(R.id.imageView10);
            GlideApp.with(CommodityActivity.this)
                    .load(mCommodityImageRef.child(String.valueOf(i)))
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .override(150,150)
                    .placeholder(R.drawable.ic_image_black_24dp)
                    .into(imageView);
            gallery.addView(view);
        }

    }

    public int getItemData(){

        long datetemp = System.currentTimeMillis();
        mAuth = FirebaseAuth.getInstance();
        SimpleDateFormat postdate = new SimpleDateFormat("yyyy-MM-dd");
        Date resultDate = new Date(datetemp);

        final String[] itemData = new String[4];
        Map<String,Object> itemfile = new HashMap<>();

        itemData[0] = etItemName.getEditText().getText().toString();
        itemData[1] = etItemPrice.getEditText().getText().toString();
        itemData[2] = tvItemLocation.getText().toString();
        itemData[3] = etItemDes.getText().toString();
        //新增
        if(!edit_status){
            mItemRef = FirebaseDatabase.getInstance().getReference().child("ItemFile").child(String.valueOf(datetemp));
            itemID = mItemRef.toString().substring(49);
            itemfile.put("ItemOwner",userID);
            itemfile.put("ItemName",itemData[0]);
            itemfile.put("ItemPrice",itemData[1]);
            itemfile.put("ItemLocation",itemData[2]);
            itemfile.put("ItemDes",itemData[3]);
            itemfile.put("PostDate",postdate.format(resultDate));
            itemfile.put("ImgCount",String.valueOf(selected.size()));
            itemfile.put("lend",false);
        }
        //修改
        else{
            mItemRef = FirebaseDatabase.getInstance().getReference().child("ItemFile").child(itemID);
            itemfile.put("ItemOwner",userID);
            itemfile.put("ItemName",itemData[0]);
            itemfile.put("ItemPrice",itemData[1]);
            itemfile.put("ItemLocation",itemData[2]);
            itemfile.put("ItemDes",itemData[3]);
            itemfile.put("PostDate",date);
            itemfile.put("lend",false);
            if (image_edit){
                itemfile.put("ImgCount",String.valueOf(selected.size()));
            }
            else{
                itemfile.put("ImgCount",String.valueOf(count));
            }
        }

        //判斷式
        String imgtemp = (String) itemfile.get("ImgCount");
        int imgcou = Integer.valueOf(imgtemp);
        if (itemData[0].length()==0 || itemData[1].length()==0 || itemData[2].length()==0 ||itemData[3].length()==0 || imgcou ==0){
            return CHECK_FAIL;
        }

        mItemRef.setValue(itemfile).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.d("Item OPLOAD","SUCCESS : ");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("Item OPLOAD","onFailure: "+e.toString());
            }
        });
        return CHECK_SUCCESS;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && data != null) {
            //获取选择器返回的数据
            selected = data.getStringArrayListExtra(ImageSelector.SELECT_RESULT);

            LayoutInflater inflater = LayoutInflater.from(this);
            for(String p:selected){
                View view =inflater.inflate(R.layout.item,gallery,false);
                ImageView imageView = view.findViewById(R.id.imageView10);
                GlideApp.with(CommodityActivity.this)
                        .load(p)
                        .placeholder(R.drawable.ic_image_black_24dp)
                        .centerInside()
                        .into(imageView);
                gallery.addView(view);
                Bitmap bitmap = BitmapFactory.decodeFile(p);//从路径加载出图片bitmap
                Context appContext = getApplicationContext();
                Uri uri = getImageUri(appContext,bitmap);
                uris.add(uri);
            }
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    private void ImageUpLoad(){
        mCommodityImageRef = FirebaseStorage.getInstance().getReference().child("CommodityPhoto").child(itemID);
        mItemRef = FirebaseDatabase.getInstance().getReference().child("ItemFile").child(itemID);

        if(uris != null){
            int count = 0;
            while(count<uris.size()){
                imageUpLoad = mCommodityImageRef.child(String.valueOf(count+1));
                imageUpLoad.putFile(uris.get(count)).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                    }
                });
                count++;
            }
            Log.d("Image Upload ","Success");
        }
        else{
            Toast.makeText(this,"No File Selected!!!",Toast.LENGTH_SHORT).show();
        }
        Common.setLoadingBar(loadingBar);
        new AlertDialog.Builder(CommodityActivity.this)
                .setTitle("商品上傳成功")
                .setMessage("可以開始愉快的交易囉~")
                .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        goback();
                    }
                })
                .show();
    }

    private void goback(){
        Intent i = new Intent(CommodityActivity.this,MyCommodityActivity.class);
        startActivity(i);
        finish();
    }
}
