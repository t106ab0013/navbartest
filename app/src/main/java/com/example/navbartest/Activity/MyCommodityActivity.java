package com.example.navbartest.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.navbartest.Common.Common;
import com.example.navbartest.GlideApp;
import com.example.navbartest.Item.Commodity;
import com.example.navbartest.Item.CommodityDecoration;
import com.example.navbartest.Item.CommodityViewHolder;
import com.example.navbartest.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class MyCommodityActivity extends AppCompatActivity {
    FirebaseAuth mAuth;
    DatabaseReference itemRef;
    Query queryRef;
    String userID;
    List<Commodity> commodityList;
    RecyclerView commodityRecyclerView;
    StorageReference itemImageRef;
    FirebaseRecyclerAdapter CommodityAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_commodity);
        setTitle("我的商品");
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        itemRef = FirebaseDatabase.getInstance().getReference().child("ItemFile");
        commodityRecyclerView = findViewById(R.id.commondityRecyclerview);
        commodityRecyclerView.setLayoutManager(new GridLayoutManager(this, 2,RecyclerView.VERTICAL,true));
        commodityRecyclerView.addItemDecoration(new CommodityDecoration(Common.dp2px(5),2));
        commodityList = new ArrayList<>();
        itemImageRef = null;

        FloatingActionButton addNewItem = findViewById(R.id.addNewitem);
        addNewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addCommodity = new Intent(MyCommodityActivity.this,CommodityActivity.class);
                startActivity(addCommodity);
            }
        });

        query();


    }

    @Override
    protected void onStart() {
        super.onStart();
        GlideApp.get(MyCommodityActivity.this).clearMemory();
        CommodityAdapter.startListening();
    }


    @Override
    protected void onStop() {
        super.onStop();
        GlideApp.get(MyCommodityActivity.this).clearMemory();
        CommodityAdapter.stopListening();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Intent toMainActivity = new Intent(MyCommodityActivity.this,MainActivity.class);
                toMainActivity.putExtra("FragmentID",4);
                startActivity(toMainActivity);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //查詢範例
    public void query(){
        queryRef = itemRef.orderByChild("ItemOwner").equalTo(userID);
        FirebaseRecyclerOptions<Commodity> options = new FirebaseRecyclerOptions.Builder<Commodity>()
                .setQuery(queryRef, new SnapshotParser<Commodity>() {
                    @NonNull
                    @Override
                    public Commodity parseSnapshot(@NonNull DataSnapshot snapshot) {
                        return new Commodity(snapshot.getKey()
                                ,(String)snapshot.child("ItemDes").getValue()
                                ,(String)snapshot.child("ItemLocation").getValue()
                                ,(String)snapshot.child("ItemName").getValue()
                                ,(String)snapshot.child("ItemOwner").getValue()
                                ,(String)snapshot.child("ItemPrice").getValue()
                                ,(String)snapshot.child("PostDate").getValue()
                                ,(String)snapshot.child("ImgCount").getValue()
                                ,(boolean)snapshot.child("lend").getValue());

                    }
                })
                .build();
        CommodityAdapter = new FirebaseRecyclerAdapter<Commodity, CommodityViewHolder>(options) {

            @NonNull
            @Override
            public CommodityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.commodity_layout,parent,false);
                return new CommodityViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull CommodityViewHolder holder, int position, @NonNull final Commodity model) {
                ViewGroup.LayoutParams para = holder.iVCommodityImage.getLayoutParams();
                para.width = (Common.getScreenWidth()-Common.dp2px(20))/2;
                para.height = (Common.getScreenWidth()-Common.dp2px(20))/2;
                holder.iVCommodityImage.setLayoutParams(para);

                itemImageRef = FirebaseStorage.getInstance().getReference().child("CommodityPhoto").child(model.getItemID()).child("1");
                GlideApp.with(MyCommodityActivity.this)
                        .load(itemImageRef)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .placeholder(R.drawable.ic_image_white_24dp)
                        .centerInside()
                        .into(holder.iVCommodityImage);
                holder.tVCommondityName.setText(model.getItemName());
                holder.tVCommondityPrice.setText("$ "+model.getItemPrice());
                if (model.getlend()){
                    holder.tVPostDate.setText("商品出借中");
                }
                else{
                    holder.tVPostDate.setText(model.getPostDate());
                }

                //OnClick
                //短按商品進編輯，長按進預覽
                holder.CommodityView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (model.getlend()){
                            /**
                             * 訂單進行中鎖住商品內容避免錯誤
                             */
                            Toast.makeText(MyCommodityActivity.this,"商品借出中! 請待訂單完成後再行修改",Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Intent intent = new Intent(MyCommodityActivity.this,CommodityActivity.class);
                            intent.putExtra("Edit_status",true);
                            intent.putExtra("ItemID",model.getItemID());
                            intent.putExtra("ItemDes",model.getItemDes());
                            intent.putExtra("ItemLocation",model.getItemLocation());
                            intent.putExtra("ItemName",model.getItemName());
                            intent.putExtra("ItemOwner",model.getItemOwner());
                            intent.putExtra("ItemPrice",model.getItemPrice());
                            intent.putExtra("ImgCount",model.getImgCount());
                            intent.putExtra("PostDate",model.getPostDate());
                            startActivity(intent);
                            finish();
                        }
                    }
                });

                holder.CommodityView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        Intent intent = new Intent(MyCommodityActivity.this,CommodityBrowserActivity.class);
                        intent.putExtra("ItemID",model.getItemID());
                        intent.putExtra("ItemDes",model.getItemDes());
                        intent.putExtra("ItemLocation",model.getItemLocation());
                        intent.putExtra("ItemName",model.getItemName());
                        intent.putExtra("ItemOwner",model.getItemOwner());
                        intent.putExtra("ItemPrice",model.getItemPrice());
                        intent.putExtra("ImgCount",model.getImgCount());
                        intent.putExtra("PostDate",model.getPostDate());
                        intent.putExtra("lend",model.getlend());
                        startActivity(intent);
                        return true;
                    }
                });

            }

            @Override
            public void onError(@NonNull DatabaseError error) {
                super.onError(error);
                Log.e("DataBase GET ", error.toString());
            }
        };
        commodityRecyclerView.setAdapter(CommodityAdapter);
    }


}
