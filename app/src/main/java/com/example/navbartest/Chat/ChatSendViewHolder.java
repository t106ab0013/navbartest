package com.example.navbartest.Chat;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.navbartest.R;

public class ChatSendViewHolder extends RecyclerView.ViewHolder {
    public TextView tvChat,tvTime;


    public ChatSendViewHolder(@NonNull View itemView) {
        super(itemView);
        tvChat = itemView.findViewById(R.id.tvChat);
        tvTime = itemView.findViewById(R.id.tvTime);
    }
}
