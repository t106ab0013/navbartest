package com.example.navbartest.Chat;

public class Chat {
    String sender,reciver,text;
    Long timestamp;

    public Chat(String sender,String reciver,String text,long timestamp){
        this.sender = sender;
        this.reciver = reciver;
        this.text = text;
        this.timestamp = timestamp;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSender() {
        return sender;
    }

    public void setReciver(String reciver) {
        this.reciver = reciver;
    }

    public String getReciver() {
        return reciver;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getTimestamp() {
        return timestamp;
    }
}
