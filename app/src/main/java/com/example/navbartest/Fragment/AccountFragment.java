package com.example.navbartest.Fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.MemoryCategory;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.navbartest.AccountSettingAdapter;
import com.example.navbartest.AccountSettingListModel;
import com.example.navbartest.Activity.LoginActivity;
import com.example.navbartest.GlideApp;
import com.example.navbartest.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {

    View v;
    RecyclerView accountSettingRecyclerview;
    List<AccountSettingListModel> aSList;
    AccountSettingAdapter accountSettingAdapter;
    TextView tVAccount;
    ImageView iVAccountFragment;
    FirebaseAuth mAuth;
    DatabaseReference mAccountRef;
    StorageReference mAccountImageRef;
    ProgressDialog loadingBar;

    public AccountFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_account, container, false);
        accountSettingRecyclerview = v.findViewById(R.id.accountSettingRecyclerview);
        accountSettingAdapter = new AccountSettingAdapter(aSList,getContext());
        accountSettingRecyclerview.setAdapter(accountSettingAdapter);
        accountSettingRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));


        tVAccount = v.findViewById(R.id.tVAccount);
        iVAccountFragment = v.findViewById(R.id.iVAccountFragment);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getInstance().getCurrentUser();
        String userID;

        if(user!=null){
            //有登入的時候，更新顯示的帳號
            String email = user.getEmail();
            tVAccount.setText(email);
            userID = mAuth.getCurrentUser().getUid();
            mAccountRef = FirebaseDatabase.getInstance().getReference().child("AccountFile").child(userID);
            mAccountImageRef = FirebaseStorage.getInstance().getReference().child("AccountPhoto").child(userID+".jpg");

            setLoadingBar(true);
            //拿到頭貼的URL並載入
            DatabaseReference mAccountPhotoRef = mAccountRef.child("AccountImageURL");
            Glide.get(getActivity()).setMemoryCategory(MemoryCategory.HIGH);

            mAccountPhotoRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    String accountPhotoURL = (String)dataSnapshot.getValue();
                    if(accountPhotoURL!=null){
                        /**
                         * 2020/07/16 LoadingBar 改由Glide的callback控制
                         */
                        GlideApp.with(AccountFragment.this)
                                .load(accountPhotoURL)
                                .override(125 ,125)
                                .placeholder(R.mipmap.ic_launcher)
                                .listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                        setLoadingBar(false);
                                        return false;
                                    }
                                })
                                .into(iVAccountFragment);
                    }
                    else{
                        GlideApp.with(AccountFragment.this)
                                .load(R.mipmap.ic_launcher)
                                .override(125,125)
                                .listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                        setLoadingBar(false);
                                        return false;
                                    }
                                })
                                .into(iVAccountFragment);

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }
        else{
            tVAccount.setText("尚未登入!!! 點我登入");
            tVAccount.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    Intent loginIntent = new Intent(getActivity(), LoginActivity.class);
                    getActivity().startActivity(loginIntent);
                }
            });
        }
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aSList = new ArrayList<>();
        aSList.add(new AccountSettingListModel(R.drawable.ic_format_list_bulleted_black_24dp,"租借清單",R.drawable.ic_keyboard_arrow_right_black_24dp));
        aSList.add(new AccountSettingListModel(R.drawable.ic_book_black_24dp,"我的商品",R.drawable.ic_keyboard_arrow_right_black_24dp));
        aSList.add(new AccountSettingListModel(R.drawable.ic_thumbs_up_down_black_24dp,"我的評價",R.drawable.ic_keyboard_arrow_right_black_24dp));
        aSList.add(new AccountSettingListModel(R.drawable.ic_settings_black_24dp,"帳號設定",R.drawable.ic_keyboard_arrow_right_black_24dp));
        aSList.add(new AccountSettingListModel(R.drawable.ic_help_black_24dp,"聯絡我們",R.drawable.ic_keyboard_arrow_right_black_24dp));
        aSList.add(new AccountSettingListModel(R.drawable.ic_logout_black_24dp,"登出",R.drawable.ic_keyboard_arrow_right_black_24dp));

    }

    public void setLoadingBar(Boolean start){
        if (start){
            loadingBar = new ProgressDialog(getActivity());
            loadingBar.setMessage("正在載入中...");
            loadingBar.show();
            loadingBar.setCanceledOnTouchOutside(false);
        }
        else{
            loadingBar.dismiss();
        }

    }

}
