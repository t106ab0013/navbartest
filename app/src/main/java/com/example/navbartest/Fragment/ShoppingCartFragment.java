package com.example.navbartest.Fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.navbartest.Activity.LoginActivity;
import com.example.navbartest.Item.Commodity;
import com.example.navbartest.R;
import com.example.navbartest.ShoppingCart.CartAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ShoppingCartFragment extends Fragment {
    FirebaseAuth mAuth;
    DatabaseReference cartRef;
    String userID;
    TextView tvEmpty;
    List<String> cart;
    List<Commodity> cartItem;
    RecyclerView shoppingCartRecyclerView;

    public ShoppingCartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_shopping_cart, container, false);

        mAuth = FirebaseAuth.getInstance();


        tvEmpty = v.findViewById(R.id.tvEmpty);
        shoppingCartRecyclerView = v.findViewById(R.id.shoppingCartRecyclerView);

        cart = new ArrayList<>();
        cartItem = new ArrayList<>();

        FirebaseUser user = mAuth.getInstance().getCurrentUser();
        if(user == null){
            Intent i = new Intent(getActivity(), LoginActivity.class);
            startActivity(i);
        }
        else{
            userID = mAuth.getCurrentUser().getUid();
            cartRef = FirebaseDatabase.getInstance().getReference().child("AccountFile").child(userID).child("cart");
            getCartData();
        }

        return v;
    }

    private void getCartData(){
        cartRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot s) {
                if (s.exists()){
                    tvEmpty.setVisibility(View.GONE);
                    for (DataSnapshot temp:s.getChildren()){
                        String IteminCart = (String)temp.getValue();
                        cart.add(IteminCart);
                        DatabaseReference itemRef = FirebaseDatabase.getInstance().getReference().child("ItemFile").child(IteminCart);
                        itemRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot model) {
                                cartItem.add(new Commodity(model.getKey()
                                        ,(String)model.child("ItemDes").getValue()
                                        ,(String)model.child("ItemLocation").getValue()
                                        ,(String)model.child("ItemName").getValue()
                                        ,(String)model.child("ItemOwner").getValue()
                                        ,(String)model.child("ItemPrice").getValue()
                                        ,(String)model.child("PostDate").getValue()
                                        ,(String)model.child("ImgCount").getValue()
                                        ,(boolean)model.child("lend").getValue()));

                                CartAdapter cartAdapter = new CartAdapter(getContext(),cartItem,cart,userID);
                                shoppingCartRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                                shoppingCartRecyclerView.setAdapter(cartAdapter);
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
                    }
                }
                else {
                    tvEmpty.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        shoppingCartRecyclerView.addOnChildAttachStateChangeListener(new RecyclerView.OnChildAttachStateChangeListener() {
            @Override
            public void onChildViewAttachedToWindow(@NonNull View view) {

            }

            @Override
            public void onChildViewDetachedFromWindow(@NonNull View view) {
                Log.d("Detached", String.valueOf(shoppingCartRecyclerView.getAdapter().getItemCount()));
                if (shoppingCartRecyclerView.getAdapter().getItemCount() == 0){
                    tvEmpty.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
