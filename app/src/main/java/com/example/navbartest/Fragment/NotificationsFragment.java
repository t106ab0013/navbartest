package com.example.navbartest.Fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.navbartest.Activity.LoginActivity;
import com.example.navbartest.Common.DialogHelper;
import com.example.navbartest.GlideApp;
import com.example.navbartest.Order.Order;
import com.example.navbartest.Order.OrderViewHolder;
import com.example.navbartest.Order.Order_Status;
import com.example.navbartest.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.firebase.ui.firestore.SnapshotParser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.SimpleDateFormat;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationsFragment extends Fragment {
    View v;
    RecyclerView Notification_Recyclerview;
    FirebaseFirestore mStore;
    TextView tvOrderOwner,tvOrderBuyer,tvEmpty;
    DocumentReference order,refuse;
    Query queryRef;
    FirebaseAuth mAuth;
    FirebaseUser user;
    String userID;
    StorageReference itemImageRef;
    FirestoreRecyclerAdapter orderAdapter;
    DatabaseReference mAccountRef;
    DatabaseReference commodityRef;
    DatabaseReference itemRef;
    ColorStateList defaultcolor;

    public NotificationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_notifications, container, false);


        mStore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getInstance().getCurrentUser();

        Notification_Recyclerview = v.findViewById(R.id.Notification_Recyclerview);
        Notification_Recyclerview.setLayoutManager(new LinearLayoutManager(getContext()));

        tvOrderOwner = v.findViewById(R.id.tvOrderOwner);
        tvOrderBuyer = v.findViewById(R.id.tvOrderBuyer);
        tvEmpty = v.findViewById(R.id.tvEmpty);

        defaultcolor = tvOrderOwner.getTextColors();

        if (user!=null){
            int mode = 1;
            query(mode);
            tvOrderBuyer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    query(1);
                }
            });

            tvOrderOwner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    query(0);
                }
            });
        }
        else{
            Intent i = new Intent(getActivity(), LoginActivity.class);
            startActivity(i);
        }



        return v;
    }

    public void query(int mode){
        userID = mAuth.getCurrentUser().getUid();
        if (mode == 1){
            tvOrderBuyer.setTextColor(ContextCompat.getColor(getContext(),R.color.colorWhite));
            tvOrderBuyer.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.text_background_1));
            tvOrderOwner.setTextColor(defaultcolor);
            tvOrderOwner.setBackground(null);
            queryRef = mStore.collection("Order").whereEqualTo("Buyer",userID);
            FireStoreadapter(queryRef,mode);
        }
        else{
            tvOrderOwner.setTextColor(ContextCompat.getColor(getContext(),R.color.colorWhite));
            tvOrderOwner.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.text_background_1));
            tvOrderBuyer.setTextColor(defaultcolor);
            tvOrderBuyer.setBackground(null);
            queryRef = mStore.collection("Order").whereEqualTo("Owner",userID);
            FireStoreadapter(queryRef,mode);
        }
        orderAdapter.startListening();

    }

    private void FireStoreadapter(Query queryRef, final int mode){
        FirestoreRecyclerOptions<Order> options = new FirestoreRecyclerOptions.Builder<Order>()
                .setQuery(queryRef, new SnapshotParser<Order>() {
                    @NonNull
                    @Override
                    public Order parseSnapshot(@NonNull DocumentSnapshot s) {
                        return new Order(s.getId()
                                ,(String) s.get("ItemID")
                                ,(String) s.get("Owner")
                                ,(String) s.get("Buyer")
                                ,(String) s.get("StartDate")
                                ,(String) s.get("EndDate")
                                ,(String) s.get("Note")
                                ,(long)s.get("Period")
                                ,(long)s.get("Price")
                                ,(long)s.get("status"));
                    }
                })
                .build();
        orderAdapter = new FirestoreRecyclerAdapter<Order, OrderViewHolder>(options) {

            @NonNull
            @Override
            public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_layout,parent,false);
                return new OrderViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull final OrderViewHolder holder, int position, @NonNull final Order model) {
                itemImageRef = FirebaseStorage.getInstance().getReference().child("CommodityPhoto").child(model.getItemID()).child("1");
                GlideApp.with(NotificationsFragment.this)
                        .load(itemImageRef)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .placeholder(R.drawable.ic_image_white_24dp)
                        .centerInside()
                        .into(holder.OrderImage);
                holder.tvOrderID.setText("訂單編號 : "+model.getOrderID());
                holder.tvOrderStatus.setText(Order_Status.getStatusDes(model.getStatus(),mode));
                holder.tvOrderDate.setText("租借期間 : "+model.getStartDate()+" ~ "+model.getEndDate());
                commodityRef = FirebaseDatabase.getInstance().getReference().child("ItemFile").child(model.getItemID());
                commodityRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                       String name = (String) snapshot.child("ItemName").getValue();
                       holder.tvItemName.setText("商品名稱 : "+name);
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

                holder.OrderItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        orderClick(mode,model);
                    }
                });

            }

            @Override
            public int getItemCount() {
                return super.getItemCount();
            }
        };
        Notification_Recyclerview.setAdapter(orderAdapter);
    }

    private void orderClick(int mode,Order model){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        String orderID = model.getOrderID();
        String itemID = model.getItemID();
        final String EndDate = model.getEndDate();
        order = mStore.collection("Order").document(orderID);
        itemRef = FirebaseDatabase.getInstance().getReference().child("ItemFile").child(itemID);
        /*
        *  1 buyer
        *  0 owner
        */
        if ((int)model.getStatus() == Order_Status.STATUS_BSEND){
            if (mode == 1){
                DialogHelper.orderDialog(getActivity(), model, mode, "訂單詳情", "確定", "取消訂單", "", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        new AlertDialog.Builder(getActivity())
                                .setTitle("訂單確認")
                                .setIcon(R.drawable.ic_warning_yellow_24dp)
                                .setMessage("確定要取消該筆租借訂單嗎 ?")
                                .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        long timestamp = System.currentTimeMillis();
                                        HashMap<String,Object> update = new HashMap<>();
                                        update.put("status",Order_Status.STATUS_BCANCEL);
                                        update.put("cancelTimeStamp",String.valueOf(format.format(timestamp)));
                                        order.update(update)
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        Toast.makeText(getActivity(),"訂單已取消",Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                    }
                                }).show();
                    }
                },null);
            }
            else{
                DialogHelper.orderDialog(getActivity(), model, mode, "訂單詳情", "同意", "婉拒出借", "回上一頁", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        new AlertDialog.Builder(getActivity())
                                .setTitle("訂單確認")
                                .setIcon(R.drawable.ic_warning_yellow_24dp)
                                .setMessage("確定要同意該筆訂單嗎 ?")
                                .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        HashMap<String,Object> update = new HashMap<>();
                                        update.put("lend",true);
                                        update.put("EndDate",EndDate);
                                        order.update("status",Order_Status.STATUS_OREPLY)
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                    }
                                                });
                                        itemRef.updateChildren(update, new DatabaseReference.CompletionListener() {
                                            @Override
                                            public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                                                Toast.makeText(getActivity(),"已同意該訂單",Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                }).show();
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        LayoutInflater l = LayoutInflater.from(getContext());
                        View view = l.inflate(R.layout.alertdialog_refuse,null);
                        final EditText etrefuse = view.findViewById(R.id.etrefuse);
                        new AlertDialog.Builder(getActivity())
                                .setTitle("訂單拒絕")
                                .setIcon(R.drawable.ic_warning_yellow_24dp)
                                .setView(view)
                                .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        long timestamp = System.currentTimeMillis();
                                        HashMap<String,Object> update = new HashMap<>();
                                        update.put("status",Order_Status.STATUS_OCANCEL);
                                        update.put("refuse",etrefuse.getText().toString());
                                        update.put("cancelTimeStamp",String.valueOf(format.format(timestamp)));
                                        order.update(update)
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        Toast.makeText(getActivity(),"已拒絕該訂單",Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                    }
                                }).show();
                    }
                }, null);
            }
        }
        else if((int)model.getStatus() == Order_Status.STATUS_OREPLY){
            if (mode == 1){
                DialogHelper.orderDialog(getActivity(), model, mode, "訂單詳情", "我已確認物品狀態無誤", "", "回上一頁"
                        , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        HashMap<String,Object> itemupdate = new HashMap<>();
                        HashMap<String,Object> orderupdate = new HashMap<>();
                        itemupdate.put("lend",true);
                        orderupdate.put("status",Order_Status.STATUS_ONRENT);
                        order.update(orderupdate).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                            }
                        });
                        itemRef.updateChildren(itemupdate).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Toast.makeText(getActivity(),"已確認商品狀況",Toast.LENGTH_SHORT).show();
                            }
                        });
                        dialog.dismiss();
                    }
                },null,null);
            }
            else{
                DialogHelper.orderDialog(getActivity(), model, mode, "訂單詳情", "確定", "", ""
                        ,null,null,null);
            }
        }
        else if ((int)model.getStatus() == Order_Status.STATUS_ONRENT){
            if (mode == 1){
                DialogHelper.orderDialog(getActivity(), model, mode, "訂單詳情", "歸還商品", "", "回上一頁"
                        , new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                new AlertDialog.Builder(getActivity())
                                        .setTitle("訂單確認")
                                        .setIcon(R.drawable.ic_warning_yellow_24dp)
                                        .setMessage("確定要歸還該商品嗎 ?")
                                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                HashMap<String,Object> update = new HashMap<>();
                                                update.put("status",Order_Status.STATUS_RETUNE);
                                                order.update(update)
                                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                            @Override
                                                            public void onSuccess(Void aVoid) {
                                                                Toast.makeText(getActivity(),"商品已歸還! 請出租者確認",Toast.LENGTH_SHORT).show();
                                                            }
                                                        });
                                            }
                                        }).show();
                            }
                        }, null, null);
            }
            else{
                DialogHelper.orderDialog(getActivity(), model, mode, "訂單詳情", "確定", "", ""
                        ,null,null,null);
            }
        }
        else if ((int)model.getStatus() == Order_Status.STATUS_RETUNE){
            if (mode == 1){
                DialogHelper.orderDialog(getActivity(), model, mode, "訂單詳情", "確定", "", ""
                        ,null,null,null);
            }
            else{
                DialogHelper.orderDialog(getActivity(), model, mode, "訂單詳情", "我已收到歸還商品", "", "回上一頁"
                        , new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                new AlertDialog.Builder(getActivity())
                                        .setTitle("訂單確認")
                                        .setIcon(R.drawable.ic_warning_yellow_24dp)
                                        .setMessage("確定要該商品狀況無誤嗎 ?")
                                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                long timestamp = System.currentTimeMillis();
                                                HashMap<String,Object> update = new HashMap<>();
                                                update.put("status",Order_Status.STATUS_COMPLETE);
                                                update.put("completeTimeStamp",String.valueOf(format.format(timestamp)));
                                                HashMap<String,Object> lend = new HashMap<>();
                                                lend.put("lend",false);
                                                order.update(update)
                                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                            @Override
                                                            public void onSuccess(Void aVoid) {

                                                            }
                                                        });
                                                itemRef.updateChildren(lend, new DatabaseReference.CompletionListener() {
                                                    @Override
                                                    public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                                                        Toast.makeText(getActivity(),"已確認商品狀況 訂單完成!",Toast.LENGTH_SHORT).show();
                                                    }
                                                });

                                            }
                                        }).show();
                            }
                        }, null, null);
            }
        }
        else if ((int)model.getStatus() == Order_Status.STATUS_COMPLETE){
            DialogHelper.orderDialog(getActivity(), model, mode, "訂單詳情", "確定", "", ""
                    ,null,null,null);
        }
        else if ((int)model.getStatus() == Order_Status.STATUS_OCANCEL){
            DialogHelper.orderDialog(getActivity(), model, mode, "訂單取消", "確定", "", "",null,null,null);
        }
        else if ((int)model.getStatus() == Order_Status.STATUS_BCANCEL){
            DialogHelper.orderDialog(getActivity(), model, mode, "訂單取消", "確定", "", "",null,null,null);
        }
        else{
            DialogHelper.simpleDialogNoListener(getActivity(),"錯誤","發生未知錯誤 請聯繫客服!","確定");
        }
    }



    @Override
    public void onStart()  {
        super.onStart();
        if(user == null){
            Intent i = new Intent(getActivity(), LoginActivity.class);
            startActivity(i);
        }

        Notification_Recyclerview.addOnChildAttachStateChangeListener(new RecyclerView.OnChildAttachStateChangeListener() {
            @Override
            public void onChildViewAttachedToWindow(@NonNull View view) {
                if (Notification_Recyclerview.getAdapter().getItemCount()==0){
                    tvEmpty.setVisibility(View.VISIBLE);
                }
                else{
                    tvEmpty.setVisibility(View.GONE);
                }
            }

            @Override
            public void onChildViewDetachedFromWindow(@NonNull View view) {
                if (Notification_Recyclerview.getAdapter().getItemCount()==0){
                    tvEmpty.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    @Override
    public void onStop() {
        super.onStop();
        if (user!=null){
            orderAdapter.stopListening();
        }

    }
}
