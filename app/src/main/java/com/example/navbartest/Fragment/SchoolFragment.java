package com.example.navbartest.Fragment;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.example.navbartest.CustomAdapter;
import com.example.navbartest.SchoolTagModel;
import com.example.navbartest.R;
import com.google.firebase.database.Query;

/**
 * A simple {@link Fragment} subclass.
 */
public class SchoolFragment extends Fragment {
    View v;
    RecyclerView recyclerView;
    List<SchoolTagModel> mList;
    CustomAdapter customAdapter;
    EditText editText;
    Button searchBtn;

    public SchoolFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_school, container, false);
        editText = v.findViewById(R.id.etSearch);
        searchBtn = v.findViewById(R.id.searchButton);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editText.getText() != null)
                {
                    String searchText = editText.getText().toString();
                    SearchSchool(searchText);
                }else{
                    Toast.makeText(getActivity(), "請輸入搜尋文字", Toast.LENGTH_LONG).show();
                }
            }
        });
        recyclerView=v.findViewById(R.id.recyclerViewId);
        customAdapter=new CustomAdapter(mList,getContext());
        recyclerView.setAdapter(customAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return v;
    }

    private void SearchSchool(String searchText) {
        List<SchoolTagModel> searchList = new ArrayList<>();
        String regex = ".*" + searchText + ".*";

        for (SchoolTagModel school : mList) {
            if(school.getText().matches(regex)){
                searchList.add(school);
            }
        }
        customAdapter=new CustomAdapter(searchList,getContext());
        recyclerView.setAdapter(customAdapter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SetSchoolItems();
    }

    /*取得學校LOGO跟名字*/
    private void SetSchoolItems(){
        mList=new ArrayList<>();
        String[] schools = getResources().getStringArray(R.array.School);
        String[] image  = getResources().getStringArray(R.array.Logo);
        Context context = this.getContext();
        for (int i=0;i<schools.length;i++){
            int imageId= context.getResources().getIdentifier(image[i],"drawable", "com.example.navbartest");

            mList.add(new SchoolTagModel(imageId, schools[i]));
        }
    }
}
