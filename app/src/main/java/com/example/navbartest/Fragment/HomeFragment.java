package com.example.navbartest.Fragment;


import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.navbartest.Activity.CommodityActivity;
import com.example.navbartest.Activity.CommodityBrowserActivity;
import com.example.navbartest.Activity.MainActivity;
import com.example.navbartest.Activity.MyCommodityActivity;
import com.example.navbartest.Common.Common;
import com.example.navbartest.GlideApp;
import com.example.navbartest.Home.HomeBannerAdapter;
import com.example.navbartest.Home.HomeBannerImgHolder;
import com.example.navbartest.Item.Commodity;
import com.example.navbartest.Item.CommodityDecoration;
import com.example.navbartest.Item.CommodityViewHolder;
import com.example.navbartest.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.wangpeiyuan.cycleviewpager2.CycleViewPager2;
import com.wangpeiyuan.cycleviewpager2.CycleViewPager2Helper;
import com.wangpeiyuan.cycleviewpager2.adapter.CyclePagerAdapter;
import com.zhpan.bannerview.BannerViewPager;
import com.zhpan.bannerview.constants.IndicatorGravity;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    View v;
    Query query;
    RecyclerView commodityRecyclerView;
    FirebaseRecyclerAdapter CommodityAdapter;
    StorageReference itemImageRef;
    ArrayList<Integer> bannerimg = new ArrayList<>();
    BannerViewPager<Integer, HomeBannerImgHolder> banner;

    private DatabaseReference databaseReference;
    private static final String TAG = HomeFragment.class.getSimpleName();

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        databaseReference = FirebaseDatabase.getInstance().getReference().child("ItemFile");
        bannerimg.add(R.drawable.banner1);
        bannerimg.add(R.drawable.banner2);
        bannerimg.add(R.drawable.banner3);
        bannerimg.add(R.drawable.banner4);
        bannerimg.add(R.drawable.banner5);
    }

    @Override
    public void onStart() {
        super.onStart();
        firebaseSearch(databaseReference);
        CommodityAdapter.startListening();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_home, container, false);
        banner = v.findViewById(R.id.banner);

        setBanner();

        commodityRecyclerView = v.findViewById(R.id.commondityRecyclerview);
        commodityRecyclerView.addItemDecoration(new CommodityDecoration(Common.dp2px(5),2));
        commodityRecyclerView.setLayoutManager(new GridLayoutManager(this.getContext(), 2,RecyclerView.VERTICAL,true));

        return v;
    }

    private void firebaseSearch(Query query) {
        FirebaseRecyclerOptions<Commodity> options = new FirebaseRecyclerOptions.Builder<Commodity>()
                .setQuery(query, new SnapshotParser<Commodity>() {
                    @NonNull
                    @Override
                    public Commodity parseSnapshot(@NonNull DataSnapshot snapshot) {
                        return new Commodity(snapshot.getKey()
                                ,(String)snapshot.child("ItemDes").getValue()
                                ,(String)snapshot.child("ItemLocation").getValue()
                                ,(String)snapshot.child("ItemName").getValue()
                                ,(String)snapshot.child("ItemOwner").getValue()
                                ,(String)snapshot.child("ItemPrice").getValue()
                                ,(String)snapshot.child("PostDate").getValue()
                                ,(String)snapshot.child("ImgCount").getValue()
                                ,(boolean)snapshot.child("lend").getValue());

                    }
                })
                .build();

        CommodityAdapter = new FirebaseRecyclerAdapter<Commodity, CommodityViewHolder>(options){

            @NonNull
            @Override
            public CommodityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.commodity_layout,parent,false);
                return new CommodityViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull CommodityViewHolder holder, int position, @NonNull final Commodity model) {
                ViewGroup.LayoutParams para = holder.iVCommodityImage.getLayoutParams();
                para.width = (Common.getScreenWidth()-Common.dp2px(20))/2;
                para.height = (Common.getScreenWidth()-Common.dp2px(20))/2;
                holder.iVCommodityImage.setLayoutParams(para);

                itemImageRef = FirebaseStorage.getInstance().getReference().child("CommodityPhoto").child(model.getItemID()).child("1");
                GlideApp.with(HomeFragment.this).load(itemImageRef).centerInside().into(holder.iVCommodityImage);
                holder.tVCommondityName.setText(model.getItemName());
                holder.tVCommondityPrice.setText("$ "+model.getItemPrice());
                if (model.getlend()){
                    holder.tVPostDate.setText("商品出借中");
                }
                else{
                    holder.tVPostDate.setText(model.getPostDate());
                }


                holder.CommodityView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), CommodityBrowserActivity.class);
                        intent.putExtra("Edit_status",true);
                        intent.putExtra("ItemID",model.getItemID());
                        intent.putExtra("ItemDes",model.getItemDes());
                        intent.putExtra("ItemLocation",model.getItemLocation());
                        intent.putExtra("ItemName",model.getItemName());
                        intent.putExtra("ItemOwner",model.getItemOwner());
                        intent.putExtra("ItemPrice",model.getItemPrice());
                        intent.putExtra("ImgCount",model.getImgCount());
                        intent.putExtra("PostDate",model.getPostDate());
                        intent.putExtra("lend",model.getlend());
                        startActivity(intent);
                    }
                });

            }


            @Override
            public void onError(@NonNull DatabaseError error) {
                super.onError(error);
                Log.e("DataBase GET ", error.toString());
            }
        };
        commodityRecyclerView.setAdapter(CommodityAdapter);
    }

    private void setBanner(){
        banner.setAdapter(new HomeBannerAdapter())
                .setInterval(3000)
                .setIndicatorStyle(0)
                .setIndicatorGravity(IndicatorGravity.END)
                .create(bannerimg);
    }
}